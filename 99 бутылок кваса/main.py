def kvas_couplets(start_bottles):
    from pymorphy2 import MorphAnalyzer
    morph = MorphAnalyzer()
    bottle = morph.parse('бутылка')[0]
    del morph
    for n in range(start_bottles, 0, -1):
        yield f'''В холодильнике {n} {bottle.make_agree_with_number(n).word} кваса. 
Возьмём одну и выпьем. 
{'Осталась' if (n - 1) % 10 == 1 and n - 1 != 11 else 'Осталось'} {n - 1} \
{bottle.make_agree_with_number(n - 1).word} кваса.'''


for couplet in kvas_couplets(99):
    print(couplet)
