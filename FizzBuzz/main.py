for num in range(int(input()), int(input()) + 1):
    if num % 15 == 0:
        print('FizzBuzz')
        continue
    if num % 3 == 0:
        print('Fizz')
        continue
    if num % 5 == 0:
        print('Buzz')
        continue
    print(num)
