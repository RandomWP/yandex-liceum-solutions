import math
d = {a: math.factorial(a) for a in range(1000001)}
for i in d:
    print(i,'!=', d.get(i))
