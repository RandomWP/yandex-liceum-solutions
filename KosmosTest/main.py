counter = 0
maximum = 150
minimum = 190
growth = input()
while growth != '!':
    growth = int(growth)
    if growth >= 150 and growth <= 190:
        counter += 1
        if growth > maximum:
            maximum = growth
        if growth < minimum:
            minimum = growth
    growth = input()
print(counter)
print(minimum, maximum)