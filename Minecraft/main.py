class BaseObject:
    def __init__(self, x, y, z):
        self._coords = [x, y, z]

    def get_coordinates(self):
        return self._coords


class Block(BaseObject):
    def shatter(self):
        self._coords = [None] * 3


class Entity(BaseObject):
    def move(self, x, y, z):
        self._coords = [x, y, z]


class Thing(BaseObject):
    pass

