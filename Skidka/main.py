sum = 0
cost = float(input())
while cost >= 0:
    if cost > 1000:
        sum += cost - cost * 0.05
    else:
        sum += cost
    cost = float(input())
print(sum)