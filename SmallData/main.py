from sys import stdin
x, y = map(int, input().split())
for line in stdin:
    data = map(int, line.split())
    data = filter(lambda el: x <= el <= y, data)
    if not data:
        print(-1)
    else:
        print(max(data) - min())

