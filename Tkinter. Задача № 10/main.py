import tkinter
from math import sqrt


def move_wrap(canvas, obj, move):
    canvas.move(obj, move[0], move[1])
    if canvas.coords(obj)[0] >= canvas.winfo_width():
        canvas.move(obj, -canvas.winfo_width(), 0)
    elif canvas.coords(obj)[0] <= 0:
        canvas.move(obj, canvas.winfo_width(), 0)
    if canvas.coords(obj)[1] >= canvas.winfo_width():
        canvas.move(obj, 0, -canvas.winfo_width())
    elif canvas.coords(obj)[1] <= 0:
        canvas.move(obj, 0, canvas.winfo_width())


def key_pressed(event):
    if event.keysym == 'space':
        canvas.coords(oval, (300, 300, 310, 310))
    elif event.keysym == 'Up':
        move_wrap(canvas, oval, (0, -10))
    elif event.keysym == 'Down':
        move_wrap(canvas, oval, (0, 10))
    elif event.keysym == 'Right':
        move_wrap(canvas, oval, (10, 0))
    elif event.keysym == 'Left':
        move_wrap(canvas, oval, (-10, 0))
    if sqrt((canvas.coords(oval)[0] - 300) ** 2 + (canvas.coords(oval)[1] - 300) ** 2) >= 100:
        canvas.itemconfig(oval, fill='red')
    else:
        canvas.itemconfig(oval, fill='green')


master = tkinter.Tk()
canvas = tkinter.Canvas(master, bg='blue', height=600, width=600)
oval = canvas.create_oval((300, 300), (310, 310), fill='green')
canvas.pack()
master.bind("<KeyPress>", key_pressed)
master.mainloop()