import tkinter as tk
from random import randint, choice
from math import sqrt


def prepare_and_start():
    global player, exit, fires, enemies, used_coords
    canvas.delete("all")
    used_coords = set()
    player_pos = smart_randcoords()
    exit_pos = smart_randcoords()
    player = canvas.create_oval(
        (player_pos[0], player_pos[1]),
        (player_pos[0] + step, player_pos[1] + step),
        fill='green'
    )
    exit = canvas.create_oval(
        (exit_pos[0], exit_pos[1]),
        (exit_pos[0] + step, exit_pos[1] + step),
        fill='black'
    )
    N_FIRES = 60  # Число клеток, заполненных огнем
    fires = []
    for i in range(N_FIRES):
        fire_pos = smart_randcoords()
        fire = canvas.create_rectangle(
            (fire_pos[0], fire_pos[1]),
            (fire_pos[0] + step, fire_pos[1] + step),
            fill='orange'
        )
        fires.append(fire)
        N_ENEMIES = 3  # Число врагов
    enemies = []
    for i in range(N_ENEMIES):
        enemy_pos = smart_randcoords()
        enemy = canvas.create_oval(
            (enemy_pos[0], enemy_pos[1]),
            (enemy_pos[0] + step, enemy_pos[1] + step),
            fill='red'
        )
        enemies.append(enemy)
    label.config(text="Найди выход")
    master.bind("<KeyPress>", key_pressed)


def move_wrap(canvas, obj, move):
    canvas.move(obj, move[0], move[1])

    if canvas.coords(obj)[0] >= N_X * step:
        canvas.move(obj, -N_X * step, 0)
    elif canvas.coords(obj)[0] < 0:
        canvas.move(obj, N_X * step, 0)

    if canvas.coords(obj)[1] >= N_Y * step:
        canvas.move(obj, 0, -N_Y * step)
    elif canvas.coords(obj)[1] < 0:
        canvas.move(obj, 0, N_Y * step)


def key_pressed(event):
    if event.keysym == 'Up':
        move_wrap(canvas, player, (0, -step))
    elif event.keysym == 'Down':
        move_wrap(canvas, player, (0, step))
    elif event.keysym == 'Right':
        move_wrap(canvas, player, (step, 0))
    elif event.keysym == 'Left':
        move_wrap(canvas, player, (-step, 0))
    for enemy in enemies:
        direction = move_towards_player(enemy)  # вызвать функцию перемещения у "врага"
        move_wrap(canvas, enemy, direction)  # произвести  перемещение.
    check_move()


def do_nothing(useless):
    return


def check_move():
    if canvas.coords(player) == canvas.coords(exit):
        label.config(text="Победа!")
        master.bind("<KeyPress>", do_nothing)
    for f in fires:
        if canvas.coords(player) == canvas.coords(f):
            label.config(text="Ты проиграл!")
            master.bind("<KeyPress>", do_nothing)
    for e in enemies:
        if canvas.coords(player) == canvas.coords(e):
            label.config(text="Ты проиграл!")
            master.bind("<KeyPress>", do_nothing)


def move_towards_player(enemy):
    dist = dist_between(canvas.coords(enemy)[:2], canvas.coords(player)[:2])
    if dist_between((canvas.coords(enemy)[0] + step, canvas.coords(enemy)[1]), canvas.coords(player)[:2]) < dist:
        return (step, 0)
    elif dist_between((canvas.coords(enemy)[0] - step, canvas.coords(enemy)[1]), canvas.coords(player)[:2]) < dist:
        return (-step, 0)
    elif dist_between((canvas.coords(enemy)[0], canvas.coords(enemy)[1] + step), canvas.coords(player)[:2]) < dist:
        return (0, step)
    elif dist_between((canvas.coords(enemy)[0], canvas.coords(enemy)[1] - step), canvas.coords(player)[:2]) < dist:
        return (0, -step)
    else:
        return choice([(step, 0), (-step, 0), (0, step), (0, -step)])


def dist_between(coords1, coords2):
    return sqrt((coords1[0] - coords2[0]) ** 2 + (coords1[1] - coords2[1]) ** 2)


def smart_randcoords():
    global used_coords
    for _ in range(1000):  # Защита от зависания, если не находится нужное число
        rand = (randint(1, N_X - 1) * step, randint(1, N_Y - 1) * step)
        if rand not in used_coords:
            break
    used_coords.add(rand)
    return rand


step = 30  # Размер клетки
N_X = 20
N_Y = 20   # Размер сетки
used_coords = set()
master = tk.Tk()
label = tk.Label(master, text="")
label.pack()
canvas = tk.Canvas(
    master, bg='#349ee0', height=N_X * step, width=N_Y * step)
canvas.pack()
restart = tk.Button(master, text="Начать заново", command=prepare_and_start)
restart.pack()
prepare_and_start()
master.mainloop()
