import tkinter as tk
from math import sin, cos, pi, gcd

n = int(input())
coords = [(int(cos(2 * pi * k / n) * 200 + 256), int(sin(2 * pi * k / n) * 200 + 256)) for k in range(1, n + 1)]
for m in range(n // 2, 1, -1):
    if gcd(m, n) == 1:
        break
else:
    m = n // (n // 2)
print(m)

master = tk.Tk()
canvas = tk.Canvas(master, bg='white', height=512, width=512)
for el in range(n):
    canvas.create_line(coords[el], coords[(el + m) % n], fill='red')
canvas.pack()
master.mainloop()
