import tkinter as tk

master = tk.Tk()
canvas = tk.Canvas(master, bg='white', height=600, width=600)
for i in range(75, 601, 75):
    canvas.create_line((i, 0), (i, 600), fill='black')
    canvas.create_line((0, i), (600, i), fill='black')
canvas.pack()
master.mainloop()