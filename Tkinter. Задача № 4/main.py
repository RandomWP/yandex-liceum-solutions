import tkinter as tk

master = tk.Tk()
canvas = tk.Canvas(master, bg='white', height=600, width=600)
isBlue = False
for i in range(75, 601, 75):
    canvas.create_line((i, 0), (i, 600), fill='black')
    canvas.create_line((0, i), (600, i), fill='black')
    for j in range(0, 151, 75):
        if isBlue:
            canvas.create_oval((i - 75, j + 375), (i, j + 450), fill='blue')
        else:
            canvas.create_oval((i - 75, j), (i, j + 75), fill='red')
        isBlue = not isBlue
canvas.pack()
master.mainloop()
