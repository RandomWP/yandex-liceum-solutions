import tkinter
from random import randint


def draw(event):
    size, x, y = randint(1, 300), randint(0, 600), randint(0, 600)
    canvas.create_oval((x, y), (x + size, y + size), fill='red')
    return


master = tkinter.Tk()
canvas = tkinter.Canvas(master, bg='blue', height=600, width=600)
canvas.pack()
master.bind("<KeyPress>", draw)
master.mainloop()
