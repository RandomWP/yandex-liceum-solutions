year = int(input())
print('Високосный' if year % 4 == 0 and year % 100 != 0 or year % 400 == 0 else 'Не високосный')