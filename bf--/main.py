mem = [0] * 30000
cmdLine = input()
p = 0
for cmd in cmdLine:
    if cmd == '.':
        print(mem[p])
    elif cmd == '+':
        mem[p] += 1
    elif cmd == '-':
        mem[p] -= 1
    elif cmd == '>':
        p += 1
    elif cmd == '<':
        p -= 1
    p = p % 30000
    mem[p] = mem[p] % 256
