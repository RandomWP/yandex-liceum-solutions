class Drawer:
    def __init__(self, draw_map, cell_size):
        self._draw_map = draw_map
        self._cell_size = cell_size
        self._colors = {number: 'black' for number in self.numbers()}

    def numbers(self):
        return sorted(set(linear(self._draw_map)))

    def set_color(self, number, color):
        self._colors[number] = color

    def size(self):
        return (self._cell_size * len(self._draw_map[0]), self._cell_size * len(self._draw_map))

    def set_cell_size(self, cell_size):
        self._cell_size = cell_size

    def draw(self):
        from PIL import Image, ImageDraw
        img = Image.new('RGB', self.size())
        dr = ImageDraw.Draw(img)
        for x, line in enumerate(self._draw_map):
            for y, num in enumerate(line):
                dr.rectangle((y * self._cell_size, x * self._cell_size,
                              (y + 1) * self._cell_size, (x + 1) * self._cell_size),
                             fill=self._colors[num])
        del dr
        return img


def linear(some_list):
    if not some_list:
        return []
    if isinstance(some_list[0], list):
        return linear(some_list[0]) + linear(some_list[1:])
    return [some_list[0]] + linear(some_list[1:])

