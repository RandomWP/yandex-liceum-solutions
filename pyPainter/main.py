def picture(file_name, width, height, sky_color='#87CEEB', grass_color='#35692D',
            house_color='#874535', window_color='#AFEDED', sun_color='#FFCF40'):
    from PIL import Image, ImageDraw
    img = Image.new('RGB', (width, height), color=sky_color)
    dr = ImageDraw.Draw(img)
    dr.rectangle((0, height * 0.8, width, height), fill=grass_color)
    dr.ellipse((width * 0.8, -height * 0.2, width * 1.2, height * 0.2), sun_color)
    dr.rectangle((width * 0.25, height * 0.5, width * 0.75, height), house_color)
    dr.polygon((width * 0.25, height * 0.5, width * 0.75, height * 0.5, width * 0.5, height * 0.3),
               house_color)
    dr.rectangle((width * 0.4, height * 0.65, width * 0.6, height * 0.85), window_color)
    img.save(file_name)

