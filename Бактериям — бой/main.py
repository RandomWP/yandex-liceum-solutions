size = int(input())
field = [[int(input()) for _ in range(size)] for _ in range(size)]
for _ in range(int(input())):
    y, x = int(input()), int(input())
    field[x][y] -= 4
    for snapX in range(-1, 2):
        for snapY in range(-1, 2):
            if 0 <= x + snapX < size and 0 <= y + snapY < size:
                field[x + snapX][y + snapY] -= 4
                if field[x + snapX][y + snapY] < 0:
                    field[x + snapX][y + snapY] = 0
for line in field:
    print(*line)
