whitelist = [input() for _ in range(int(input()))]
print(*[el for el in [input() for _ in range(int(input()))] if el in whitelist], sep='\n')
