lines = int(input())
rows = [[1]]
for _ in range(lines - 1):
    rows.append([1] + [rows[-1][el] + rows[-1][el + 1] for el in range(len(rows[-1]) - 1)] + [1])
for row in rows:
    print(*row)
