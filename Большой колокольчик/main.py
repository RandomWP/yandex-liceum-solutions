class BigBell:
    def __init__(self):
        self.__flag = False

    def sound(self):
        print('dong' if self.__flag else 'ding')
        self.__flag = not self.__flag

