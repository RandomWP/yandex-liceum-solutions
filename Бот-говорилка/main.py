import random


def play_nim():
    stones = int(input('Введите начальное количество камней\n'))
    userTake = 0
    while stones > 0:
        # Program math block
        if userTake == 0:
            progTake = 1 if stones % 4 == 0 else stones % 4
        elif stones <= 3:
            progTake = stones
        else:
            progTake = 4 - userTake
        stones -= progTake

        print(botName + ' берёт:', progTake, 'камней')
        print('Осталось камней:', stones)

        # Win checking
        if stones <= 0:
            print(botName + ' выиграла!')
            break

        # User input block
        while True:
            userTake = int(input('Сколько камней вы хотите взять?\n'))
            if userTake <= 3 and userTake <= stones and userTake > 0:
                stones -= userTake
                break
            print('Можно брать не меньше 1 и не больше 3 камней и не больше, чем камней в куче!')

        print('Осталось камней', stones)

        # Win checking
        if stones <= 0:
            print('Вы выиграли!')
            break


def lichnost_test():
    print('Какое ваше любимое время года?')
    ans1 = input()
    if ans1 in ('Зима', 'Весна', 'Лето', 'Осень'):
        print('Android, iOS или Windows Mobile?')
        ans2 = input()
        if ans2 in ('Android', 'iOS', 'Windows Mobile'):
            if ans1 == 'Зима':
                if ans2 == 'Android':
                    print('Вы обладаете незаурядным умом')
                elif ans2 == 'iOS':
                    print('Вы - æàëåòü')
                elif ans2 == 'Windows Mobile':
                    print('Вы - Python')
            elif ans1 == 'Весна':
                if ans2 == 'Android':
                    print('Вы ничем не отличаетесь от других людей')
                elif ans2 == 'iOS':
                    print('Вы успешно поступите в Яндекс.Лицей')
                elif ans2 == 'Windows Mobile':
                    print('Вы - кот')
            elif ans1 == 'Лето':
                if ans2 == 'Android':
                    print('Вы существуете')
                elif ans2 == 'iOS':
                    print('Вы виртуальны')
                elif ans2 == 'Windows Mobile':
                    print('Вы находитесь в настоящем времени')
            elif ans1 == 'Осень':
                if ans2 == 'Android':
                    print('Вы не существуете')
                elif ans2 == 'iOS':
                    print('Вы - человек')
                elif ans2 == 'Windows Mobile':
                    print('Вы глупый человек')
        else:
            print('ОШИБКА')
    else:
        print('ОШИБКА')


def calculator():
    num1 = float(input('Введите первое число\n'))
    num2 = float(input('Введите вротое число\n'))
    op = input('Введие операцию (+ , -, *, /)')
    try:
        if op == '+':
            print(num1 + num2)
        elif op == '-':
            print(num1 - num2)
        elif op == '*':
            print(num1 * num2)
        elif op == '/':
            print(num1 / num2)
        else:
            print('ОШИБКА')
    except ZeroDivisionError:
        print('ОШИБКА, деление на ноль')


random.seed()
botName = random.choice(('Аква', 'Лалатина', 'Алиса', 'Рей', 'Аска', 'Хиёри'))
print('Здравствуйте, я бот-говорилка! Меня зовут ' + botName)
userName = input('А как Вас зовут?\n')
print('''Какое красивое имя! Приятно познакомится!
Давайте немого поболтаем!
Как Ваши дела?''')

while True:
    phrase = input()
    phrase = phrase.lower()

    if any(i in phrase for i in ('ужасн', 'плох', 'пипец')):
        print('Ничего, всё ещё наладится!')

    elif any(i in phrase for i in ('хорош', 'крут', 'отличн', 'норм', 'топ')):
        print('Отлично, у меня тоже всё ' +
              random.choice(('хорошо', 'круто', 'отлично', 'нормально')) + '!')

    elif any(i in phrase for i in ('ним', 'игра')):
        while True:
            phrase = input('Хотите сыграть в Ним?\n')
            phrase = phrase.lower()
            if 'да' in phrase:
                print('Загружаю игру "Ним"...\n')
                play_nim()
                break
            elif 'нет' in phrase:
                break
            else:
                print('Я не совсем поняла Ваш ответ, попробуйте снова')

    elif all(i in phrase for i in ('тест', 'личност')):
        while True:
            phrase = input('Хотите пройти тест личности?\n')
            phrase = phrase.lower()
            if 'да' in phrase:
                print('Начинаю личностный тест...\n')
                lichnost_test()
                break
            elif 'нет' in phrase:
                break
            else:
                print('Я не совсем поняла Ваш ответ, попробуйте снова')

    elif any(i in phrase for i in ('пока', 'до свидания', 'прощай')):
        print('Отлично поболтали, ' + userName + ' Ещё увидимся!')
        break

    elif all(i in phrase for i in ('открой', 'калькулятор')):
        while True:
            phrase = input('Открыть калькулятор?\n')
            phrase = phrase.lower()
            if 'да' in phrase:
                print('Открываю калькулятор...\n')
                calculator()
                break
            elif 'нет' in phrase:
                break
            else:
                print('Я не совсем поняла Ваш ответ, попробуйте снова')

    elif 'что ты умеешь' in phrase:
        print('''Я бот-говорилка.
Всегда с удовольствием поговорю с вами!
Могу поиграть с Вами в Ним, провести личностный тест или посчитать что-нибудь
(напишите: "открой калькулятор").''')

    elif '?' in phrase:
        print('К сожалению, я не могу распознавать вопросы (')

    else:
        print('Я не совсем поняла, что Вы хотели сказать... (')