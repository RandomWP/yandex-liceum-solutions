strings = [input() for _ in range(int(input()))]
letterPos = int(input())
print(*[string[letterPos - 1:letterPos] for string in strings], sep='')
