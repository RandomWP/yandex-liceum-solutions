profileName, profileDates = '', ''


def setup_profile(name, vacationDates):
    global profileName, profileDates
    profileName, profileDates = name, vacationDates


def print_application_for_leave():
    print(f'Заявление на отпуск в период {profileDates}. {profileName}')


def print_holiday_money_claim(amount):
    print(f'Прошу выплатить {amount} отпускных денег. {profileName}')


def print_attorney_letter(toWhom):
    print(f'На время отпуска в период {profileDates} '
          f'моим заместителем назначается {toWhom}. {profileName}')
