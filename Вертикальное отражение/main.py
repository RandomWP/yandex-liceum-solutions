def mirror():
    from PIL import Image
    with Image.open('image.jpg') as img:
        pixels = img.load()
        x, y = img.size
        for i in range(x // 2):
            for j in range(y):
                pixels[x - i - 1, j], pixels[i, j] = pixels[i, j], pixels[x - i - 1, j]
        img.save('res.jpg')
