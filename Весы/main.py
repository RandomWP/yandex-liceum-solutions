class Balance:
    __left = 0
    __right = 0

    def add_left(self, value):
        self.__left += value

    def add_right(self, value):
        self.__right += value

    def result(self):
        if self.__left > self.__right:
            return 'L'
        elif self.__right > self.__left:
            return 'R'
        else:
            return '='

