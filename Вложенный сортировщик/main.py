def sorted2(data, key=lambda el: -el):
    for line in data:
        line.sort(key=key)
    data = sorted(data, key=lambda x: key(x[0]))
    return data
