from sys import stdin


def smart_randpop(lst, excl):
    from random import randrange
    rand = randrange(len(lst))
    return lst.pop(rand) if lst[rand] != excl else smart_randpop(lst, excl)


names = tuple(map(lambda el: el.strip(), stdin))
friends = list(names)
for name in names:
    print(f'{name} - {smart_randpop(friends, name)}')
