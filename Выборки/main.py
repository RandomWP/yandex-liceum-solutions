class Selector:
    def __init__(self, lst):
        self.__lst = lst[:]

    def get_odds(self):
        return list(filter(lambda el: el % 2, self.__lst))

    def get_evens(self):
        return list(filter(lambda el: not el % 2, self.__lst))

