class Paragraph:
    def __init__(self, width):
        self.__width = width
        self.__lines = []

    def add_word(self, word):
        if not self.__lines or sum(map(len, self.__lines[-1])) + len(word) + \
                len(self.__lines[-1]) > self.__width:
            self.__lines.append([word])
        else:
            self.__lines[-1].append(word)

    @property
    def lines(self):
        return self.__lines

    @property
    def width(self):
        return self.__width


class LeftParagraph(Paragraph):
    def end(self):
        for line in self.lines:
            print(' '.join(line).ljust(self.width))
        self.__init__(self.width)


class RightParagraph(Paragraph):
    def end(self):
        for line in self.lines:
            print(' '.join(line).rjust(self.width))
        self.__init__(self.width)

