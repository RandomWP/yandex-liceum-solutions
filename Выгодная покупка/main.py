from sys import stdin
names = input().strip('\t').split('\t')
shops = {line.split('\t')[0]: tuple(map(int, line.strip('\n').split('\t')[1:])) for line in stdin}
min_shop = min(shops.items(), key=lambda el: sum(el[1]))[0]
print(min_shop)
print(*map('\t'.join, zip(names, map(str, shops[min_shop]))), sep='\n')

