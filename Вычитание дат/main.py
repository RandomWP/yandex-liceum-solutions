class Date:
    def __init__(self, month, day):
        self.__DAYS = dict(zip(range(1, 13),
                               (0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334)))
        self.__date = (month, day)

    @property
    def date(self):
        return self.__date

    def __sub__(self, other):
        return (self.__DAYS[self.date[0]] + self.date[1]) - \
                (other.__DAYS[other.date[0]] + other.date[1])

