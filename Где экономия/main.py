size = int(input())
matrix = [[0] * size]
for lineNum in range(size - 1):
    line = [int(val) for val in input().split()]
    line += [0] * (size - lineNum - 1)
    matrix.append(line)
for x in range(size):
    for y in range(size):
        matrix[x][y] = matrix[y][x]
econs = []
for start in range(size):
    for des in range(start + 1, size):
        econ = start
        for st in range(size):
            if st == start:
                continue
            if matrix[start][st] + matrix[st][des] < matrix[start][econ] + matrix[econ][des]:
                econ = st
        if econ != start:
            econs.append(sorted([start, des]))
print(*[' '.join([str(i) for i in el]) for el in sorted(econs)], sep='\n')
