from string import ascii_lowercase, digits, ascii_uppercase
LETTERS_LOWER = ''.join((map(lambda el: '' if el in 'lI10oO' else el, ascii_lowercase)))
LETTERS_UPPER = ''.join((map(lambda el: '' if el in 'lI10oO' else el, ascii_uppercase)))
DIGITS = ''.join((map(lambda el: '' if el in 'lI10oO' else el, digits)))
ALPHABET = LETTERS_LOWER + LETTERS_UPPER + DIGITS


def randpop(lst):
    from random import randrange
    return lst.pop(randrange(len(lst)))


def generate_password(m):
    from random import choice
    rand = list(range(m))
    alp = list(ALPHABET)
    pswd = list(map(lambda a: randpop(alp), range(m)))
    for tst in [DIGITS, LETTERS_UPPER, LETTERS_LOWER]:
        if any(map(lambda el: el not in pswd, tst)):
            letter = choice(tst)
            while letter in pswd:
                letter = choice(tst)
            pswd[randpop(rand)] = letter
    return ''.join(pswd)


def main(n, m):
    passwords = []
    while len(passwords) < n:
        password = generate_password(m)
        if password not in passwords:
            passwords.append(password)
    return passwords
