from string import ascii_letters, digits
ALPHABET = ''.join((map(lambda el: '' if el in 'lI10oO' else el, ascii_letters + digits)))


def generate_password(m, psw=''):
    from random import choice
    if len(psw) == m:
        return psw
    return generate_password(m, psw + choice(ALPHABET))


def main(n, m):
    passwords = []
    while len(passwords) < n:
        password = generate_password(m)
        if password not in passwords:
            passwords.append(password)
    return passwords
