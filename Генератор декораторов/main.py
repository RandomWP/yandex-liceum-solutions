def check_password(password):
    def password_checker(func):
        def wrapped_func(*args, **kwargs):
            inp_pass = input('Input password:')
            if inp_pass == password:
                return func(*args, **kwargs)
            print('Incorrect password')
            return
        return wrapped_func
    return password_checker

