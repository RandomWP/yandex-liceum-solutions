from swift import words
from random import choice
words = tuple(map(lambda el: el.lower(), filter(lambda el: el not in ',.?!;:-', words)))
count = int(input())
sentence = [choice(words)]
for _ in range(count - 1):
    lastWordIns = filter(lambda el: words[el] == sentence[-1], range(len(words)))
    possibNextWords = tuple(map(lambda el: words[el + 1], lastWordIns))
    sentence.append(choice(possibNextWords))
print(' '.join(sentence).capitalize() + '.')

