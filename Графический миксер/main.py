def twist_image(input_ﬁle_name, output_ﬁle_name):
    from PIL import Image
    img = Image.open(input_ﬁle_name)
    x, y = img.size
    part1 = img.crop((0, 0, x // 2, y))
    part2 = img.crop((x // 2, 0, x, y))
    img.paste(part1, (x // 2, 0))
    img.paste(part2, (0, 0))
    img.save(output_ﬁle_name + '.jpg')

