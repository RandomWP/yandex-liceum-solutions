strings = [input() for _ in range(int(input()))]
for string in strings:
    flag = True
    for currChar in range(len(string[::2])):
        for char in range(len(string[2::2])):
            if currChar == char:
                continue
            if string[::2][currChar] == string[::2][char]:
                flag = False
    oddChars = string[1::2]
    if flag:
        flag = oddChars == oddChars[::-1]
    if flag:
        print(string)
