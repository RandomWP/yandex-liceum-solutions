vals = [int(input()) for _ in range(int(input()))]
minVal = vals[0]
maxVal = vals[-1]
print(*[val for val in vals[1:-1] if maxVal >= val >= minVal], sep='\n')
