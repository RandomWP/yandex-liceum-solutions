def cached(func):
    cache = {}

    def wrapped(*args, **kwargs):
        nonlocal cache
        if (args, tuple(kwargs.items())) in cache:
            return cache[(args, tuple(kwargs.items()))]
        result = func(*args, **kwargs)
        cache[(args, tuple(kwargs.items()))] = result
        return result
    return wrapped

