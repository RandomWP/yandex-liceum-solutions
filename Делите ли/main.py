num = int(input())
delCount = 0
for i in range(1, num + 1):
    if num % i == 0:
        delCount += 1
        if i != num:
            print(str(i) + ' ', end='')
        else:
            print(i)
if delCount == 2:
    print('ПРОСТОЕ')
else:
    print('НЕТ')