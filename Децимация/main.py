names = [input() for _ in range(int(input()))]
num = int(input())
cycles = int(input())
for cycle in range(cycles):
    names = [names[el] for el in range(num - 1, len(names)) if el + 1 % num != 0]
print(*names, sep='\n')
