def mirror():
    from PIL import Image, ImageOps
    ImageOps.mirror(Image.open('image.jpg').rotate(90)).save('res.jpg')
