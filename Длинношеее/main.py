def print_long_words(text):
    text = ''.join([char if char.isalpha() else ' ' for char in text])
    words = [word.lower() for word in text.split()]
    glas = 'аоэиуыеёюяaeiouy'
    words = [word for word in words if len([char for char in word if char in glas]) > 3]
    print(*words, sep='\n')
