checkNum = 1
goods = []
costs = []


def add_item(itemName, itemCost):
    global goods, costs
    goods.append(itemName)
    costs.append(itemCost)


def print_receipt():
    global goods, costs, checkNum
    if not goods:
        return
    print(f'Чек {checkNum}. Всего предметов: {len(goods)}')
    for i, good in enumerate(goods):
        print(f'{good} - {costs[i]}')
    print(f'Итого: {sum(costs)}')
    print('-----')
    checkNum += 1
    goods, costs = [], []
