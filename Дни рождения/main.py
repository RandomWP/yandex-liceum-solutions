dict = {}
for _ in range(int(input())):
    temp = input().split()
    if temp[-1] in dict.keys():
        dict[temp[-1]].append(temp[0])
    else:
        dict[temp[-1]] = [temp[0]]
print(*[' '.join(sorted(dict.get(input(), ''))) for _ in range(int(input()))], sep='\n')
