def markdown_to_docx(text):
    from docx import Document
    lines = text.split('\n')
    docname = lines[0].strip()
    lines = map(lambda line: line.rstrip(), lines[1:])
    doc = Document()
    doc.add_heading(docname, 0)
    first_empty_line = True
    for line in lines:
        styles = []
        if not line.strip() and first_empty_line:
            first_empty_line = False
            continue
        elif not line.strip():
            doc.add_paragraph()
            continue
        else:
            first_empty_line = True

        if line.startswith('#'):
            doc.add_heading(line.strip('# '), len(line.split(' ')[0]))
        elif line.split('.')[0].isdigit():
            parse_styles(doc.add_paragraph(style='List Number'), line.lstrip('1234567890.'), styles)
        elif line[0] in '+-*' and line[1] == ' ':
            parse_styles(doc.add_paragraph(style='List Bullet'), line[2:], styles)
        else:
            parse_styles(doc.add_paragraph(), line, styles)
    doc.save('res.docx')


def parse_styles(paragraph, line, styles):
    if not line.strip('_*'):
        return
    if line[0] in '_*':
        style_type = line[:3].count(line[0])
        if styles and styles[-1] == style_type:
            del styles[-1]
            parse_styles(paragraph, line[style_type:], styles)
        else:
            line = line[style_type:]
            style_marker = find_next_style(line)
            if styles:
                style_type += styles[-1]
            styles.append(style_type - styles[-1] if styles else style_type)
            set_style(paragraph.add_run(line[:style_marker]), style_type)
            parse_styles(paragraph, line[style_marker:], styles)
    else:
        style_type = styles[-1] if styles else None
        style_marker = find_next_style(line)
        set_style(paragraph.add_run(line[:style_marker]), style_type)
        parse_styles(paragraph, line[style_marker:], styles)


def find_next_style(line):
    return min(line.find('*') if '*' in line else len(line),
               (line.find('_') if '_' in line else len(line)))


def set_style(run, style_type):
    if style_type == 1 or style_type == 3:
        run.italic = True
    if style_type == 2 or style_type == 3:
        run.bold = True

