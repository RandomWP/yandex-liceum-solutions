class BaseCharacter:
    def __init__(self, pos_x, pos_y, hp):
        self._pos = [pos_x, pos_y]
        self._hp = hp

    def move(self, delta_x, delta_y):
        self._pos[0] += delta_x
        self._pos[1] += delta_y

    def is_alive(self):
        return self._hp > 0

    def get_damage(self, amount):
        self._hp -= amount

    def get_coords(self):
        return tuple(self._pos)


class BaseEnemy(BaseCharacter):
    def __init__(self, pos_x, pos_y, weapon, hp):
        self._weapon = weapon
        super().__init__(pos_x, pos_y, hp)

    def hit(self, target):
        if not isinstance(target, MainHero):
            print('Могу ударить только Главного героя')
        else:
            self._weapon.hit(self, target)

    def __str__(self):
        return f'Враг на позиции {tuple(self._pos)} с оружием {self._weapon}'


class MainHero(BaseCharacter):
    def __init__(self, pos_x, pos_y, name, hp):
        self._name = name
        self._current_weapon = 0
        self._weapons = []
        super().__init__(pos_x, pos_y, hp)

    def hit(self, target):
        if not self._weapons:
            print('Я безоружен')
        elif not isinstance(target, BaseEnemy):
            print('Могу ударить только Врага')
        else:
            self._weapons[self._current_weapon].hit(self, target)

    def add_weapon(self, weapon):
        if not isinstance(weapon, Weapon):
            print('Это не оружие')
        else:
            print(f'Подобрал {weapon}')
            self._weapons.append(weapon)

    def next_weapon(self):
        if not self._weapons:
            print('Я безоружен')
        elif len(self._weapons) == 1:
            print('У меня только одно оружие')
        else:
            if self._current_weapon + 1 == len(self._weapons):
                self._current_weapon = 0
            else:
                self._current_weapon += 1
            print(f'Сменил оружие на {self._weapons[self._current_weapon]}')

    def heal(self, amount):
        self._hp += amount
        if self._hp > 200:
            self._hp = 200
        print(f'Полечился, теперь здоровья {self._hp}')


class Weapon:
    def __init__(self, name, damage, range):
        self._name = name
        self._damage = damage
        self._range = range

    def hit(self, actor, target):
        if not target.is_alive():
            print('Враг уже повержен')
        elif distance_between(actor, target) > float(self._range):
            print(f'Враг слишком далеко для оружия {self._name}')
        else:
            print(f'Врагу нанесен урон оружием {self._name} в размере {self._damage}')
            target.get_damage(self._damage)

    def __str__(self):
        return self._name


def distance_between(character1, character2):
    from math import sqrt
    x, y = map(lambda el: el[0] - el[1], zip(character1.get_coords(), character2.get_coords()))
    return sqrt(x * x + y * y)

