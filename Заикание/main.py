lastMessage = ''


def print_without_duplicates(message):
    global lastMessage
    if message != lastMessage:
        print(message)
        lastMessage = message
