def fib(n):
    a, b = 0, 1
    c = 1
    for i in range(2, n + 1):
        c = a + b
        a, b = b, c
    return c


def golden_ratio(i):
    print(fib(i + 1) / fib(i))
