def masha(num):
    return num * 3 - 2


def petya(num):
    while num % 2 == 0:
        num //= 2
    while num % 3 == 0:
        num //= 3
    return num + 11


numbers = [int(input())]
stepFlag = input() == 'Петя'
for step in range(int(input())):
    numbers.insert(0, petya(numbers[0]) if stepFlag else masha(numbers[0]))
    if step == 0:
        del numbers[-1]
    stepFlag = not stepFlag
numbers.sort()
print(numbers[len(numbers) // 2] if len(numbers) % 2 != 0
      else (numbers[len(numbers) // 2 - 1] + numbers[len(numbers) // 2]) / 2)
