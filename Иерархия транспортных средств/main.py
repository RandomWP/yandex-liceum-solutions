class Vehicle:
    pass


class WaterVehicle(Vehicle):
    pass


class AirVehicle(Vehicle):
    pass


class Plane(AirVehicle):
    pass


class AerostaticVehicle(AirVehicle):
    pass


class Baloon(AerostaticVehicle):
    pass


class Airship(AerostaticVehicle):
    pass


class GroundVehicle(Vehicle):
    pass


class RailroadVehicle(GroundVehicle):
    pass


class Automobile(GroundVehicle):
    pass


class AnimalPoweredVehicle(GroundVehicle):
    pass


class Bicycle(GroundVehicle):
    pass


class SpaceVehicle(Vehicle):
    pass


class DeathStar(SpaceVehicle):
    pass

