string = input()
pos = int(input())
letter = input()
if len(letter) == 1 and 0 < pos <= len(string):
    print('ДА' if letter == string[pos - 1] else 'НЕТ')
else:
    print('ОШИБКА')
