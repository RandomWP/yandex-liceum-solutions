points = []
maxx = 0
for pNum in range(int(input())):
    point = tuple((int(el) // 10) for el in input().split())
    p = 0
    points.append(point)
    p = points.count(point)
    if p > maxx:
        maxx = p
print(maxx)
