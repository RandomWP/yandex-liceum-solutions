class SquareFunction:
    def __init__(self, a, b, c):
        self.__a = a
        self.__b = b
        self.__c = c

    def __call__(self, x):
        return self.__a * x * x + self.__b * x + self.__c

