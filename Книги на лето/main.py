homeBooks = set()
homeBooksCount = int(input())
listBooksCount = int(input())
homeBooks = {input() for _ in range(homeBooksCount)}
for _ in range(listBooksCount):
    print('YES' if input() in homeBooks else 'NO')
