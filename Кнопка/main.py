class Button:
    __clicks = 0

    def click(self):
        self.__clicks += 1

    def click_count(self):
        return self.__clicks

    def reset(self):
        self.__clicks = 0

