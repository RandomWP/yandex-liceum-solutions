def ask_password(login, password, success, failure):
    error = ''
    login, password = login.lower(), password.lower()
    if len([char for char in password if char in 'aeiouy']) != 3:
        error = 'Wrong number of vowels'
    if [char for char in login if char.isalpha() and char not in 'aeiouy'] != \
            [char for char in password if char.isalpha() and char not in 'aeiouy']:
        error = 'Wrong consonants' if not error else 'Everything is wrong'
    if not error:
        success(login)
    else:
        failure(login, error)


def ok(login):
    print(f'Привет, {login}!')


def fail(login, err):
    print(f'Кто-то пытался притвориться пользователем {login}, '
          f'но в пароле допустил ошибку: {err.upper()}.')


def main(login, password):
    ask_password(login, password, ok, fail)
