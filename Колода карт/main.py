from itertools import product, chain
suits = ['пик', 'треф', 'бубен', 'червей']
suits.remove(input())
values = map(str, chain(range(2, 11), ('валет', 'дама', 'король', 'туз')))
print(*(' '.join(el) for el in product(values, suits)), sep='\n')

