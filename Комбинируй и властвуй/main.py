answer = sum(map(lambda num: num ** 2, filter(lambda num: num % 9 == 0, range(10, 100))))
print(answer)  # Ответ 40905
