num = int(input()[1:])
lines = []
for _ in range(num):
    line = input()
    lines.append(line[:line.find('#') if line.find('#') >= 0 else len(line)].rstrip())
print(*lines, sep='\n')
