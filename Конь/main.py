class ChessPiece:
    def __init__(self, row, col, color):
        self._row = row
        self._col = col
        self._color = color

    def set_position(self, row, col):
        self._row = row
        self._col = col

    def check_board(self, row, col):
        return 0 <= row < 8 and 0 <= col < 8

    def char(self):
        return self.__class__.__name__[0]

    def get_color(self):
        return self._color

    def can_move(self, row, col):
        return self.check_board(row, col)


class Knight(ChessPiece):
    def char(self):
        return 'N'

    def can_move(self, row, col):
        flag = False
        for row_shift in [-2, -1, 1, 2]:
            for col_shift in [-2, -1, 1, 2]:
                if abs(row_shift) == abs(col_shift):
                    continue
                if self._row + row_shift == row and self._col + col_shift == col:
                    flag = True
                    break
            else:
                continue
            break
        return self.check_board(row, col) and flag

