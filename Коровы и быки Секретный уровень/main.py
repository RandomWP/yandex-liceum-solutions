class CowBullNum:
    __history = {}  # История попыток
    __win = False  # Флаг выигрыша

    def __init__(self):  # Коструктор класса
        self.__num = self.__generate_num()  # При создании объекта загадываем случайное число

    def print_num(self):  # Метод для вывода загаданного числа (отладочный)
        print(''.join(map(str, self.__num)))

    def __num_to_diglist(self, num):  # Метод для преобразования числа в список цифр
        return tuple(map(int, str(num)))

    def user_input(self):  # Метод для ввода попыток пользователя
        while True:  # Защита от ввода неверных чисел
            num = self.__num_to_diglist(int(input('Введите число: ')))
            if len(set(num)) == 4 and len(num) == 4:
                break
            print('В числе не 4 знака или цифры повторяются')
        bulls, cows = self.__count_bulls_cows(self.__num, num)  # Подсчет количества быков и коров
        self.__history[num] = (bulls, cows)  # Добавляем попытку в историю
        if bulls == 4:  # Если все цифры совпали, ставим флаг выигрыша
            self.__win = True
            return bulls, cows
        self.__randomize_num()  # Обновляем загаданное число
        return bulls, cows

    def __count_bulls_cows(self, num, ans):  # Метод для подсчета бфков и коров
        bulls, cows = 0, 0
        for dig, ans_dig in zip(num, ans):
            if dig == ans_dig:
                bulls += 1
            elif ans_dig in num:
                cows += 1
        return bulls, cows

    def __generate_num(self):  # Метод для генерации случайного числа согласно правилам игры
        from random import sample, randrange
        new_num = sample((0, 1, 2, 3, 4, 5, 6, 7, 8, 9), 4)  # Число - 4 неповторяющиеся цифры
        if new_num[0] == 0:  # Если первая цифра - 0, меняем её местами с другой случайной цифрой
            r = randrange(1, 4)
            new_num[0], new_num[r] = new_num[r], new_num[0]
        return new_num

    def __check_history(self, num):  # Метод для проверки соответсвия числа истории попыток
        for n, (h_bulls, h_cows) in self.__history.items():
            bulls, cows = self.__count_bulls_cows(num, n)
            if h_bulls != bulls or h_cows != cows:
                return False
        return True

    def __randomize_num(self):  # Метод для подмены загаданного числа
        while True:
            new_num = self.__generate_num()  # Генерируем новое число
            if self.__check_history(new_num):  # Проверяем на соответствие истории попыток
                break
        self.__num = new_num

    def check_win(self):  # Метод, возвращающий флаг выигрыша (инкапсуляция же)
        return self.__win


def main():
    print('Добро пожаловать в игру "Коровы и быки"')
    print('Правила просты: компьютер загадывает четырезначное число (цифры не повторяются)')
    print('Вы должны отгадать это число как можно скорее')
    print('После каждой попытки компьютер сообщает количество быков и коров')
    print('\nЕсли в названном числе цифра какого-то разряда'
          ' совпала с цифрой в том же разряде правильного ответа, это называется «быком»')
    print('Если указанная цифра есть в ответе, но на неверной позиции, это «корова»\n')
    print('Нужно вводить только четырехзначные числа с несовпадающими цифрами')
    print('Этот бот не так прост, как кажется\n')
    num = CowBullNum()  # Создаём новый объект - число для игры
    steps = 0  # Счетчик ходов
    while not num.check_win():  # Пока флаг выигрыша опущен
        bulls, cows = num.user_input()  # Запрашиваем новую попытку
        print(f'Быков: {bulls} Коров: {cows}')  # Сообщаем количество быков и коров
        steps += 1
    print(f'Вы выиграли за {steps} ходов')  # Сообщаеп о выигрыше


main()
