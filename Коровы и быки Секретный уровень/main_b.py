# encoding=utf8

class CowBullNum:
    __num = []
    __lockedPos = [None, None, None, None]
    __lockedNum = []
    __win = False

    def __init__(self):
        from random import sample, randrange
        self.__num = sample((0, 1, 2, 3, 4, 5, 6, 7, 8, 9), 4)
        if self.__num[0] == 0:
            r = randrange(1, 4)
            self.__num[0], self.__num[r] = self.__num[r], self.__num[0]


    def print_num(self):
        print(''.join(map(str, self.__num)))


    def __num_to_diglist(self, num):
        return tuple(map(int, str(num)))

    def user_input(self):
        while True:
            num = self.__num_to_diglist(int(input('Введите число: ')))
            if len(set(num)) == 4:
                break
            print('В числе не 4 знака или цифры повторяются')
        cows, bulls = 0, 0
        for n, dig, usr_dig in zip(range(4), self.__num, num):
            if dig == usr_dig:
                bulls += 1
                self.__lockedPos[n] = dig
            elif usr_dig in self.__num:
                cows += 1
                self.__lockedNum.append(usr_dig)
        if bulls == 4:
            self.__win = True
            return cows, bulls
        self.__randomize_num()
        return bulls, cows


    def __randomize_num(self):
        from random import randrange
        new_num = self.__lockedPos
        lNum = sorted(self.__lockedNum)
        for n, dig in enumerate(new_num):
            if dig is None:
                if lNum:
                    new_num[n] = lNum[1:].pop(randrange(len(lNum[1:]))) if 0 in lNum and n == 0 else lNum.pop(randrange(len(lNum)))
                else:
                    new_num[n] = randrange(1, 10) if n == 0 else randrange(0, 10)
        self.__num = new_num


    def check_win(self):
        return self.__win


def main():
    print('Добро пожаловать в игру "Коровы и быки"')
    print('Правила просты: компьютер загадывает четырезначное число (цифры не повторяются)')
    print('Вы должны отгадать это число как можно скорее')
    print('После каждой попытки компьютер сообщает количество быков и коров')
    print('\nЕсли в названном числе цифра какого-то разряда совпала с цифрой в том же разряде правильного ответа, это называется «быком»')
    print('Если указанная цифра есть в ответе, но на неверной позиции, это «корова»\n')
    print('Нужно вводить только четырехзначные числа с несовпадающими цифрами')
    print('Этот бот не так прост, как кажется\n')
    num = CowBullNum()
    steps = 0
    while not num.check_win():
        bulls, cows = num.user_input()
        print(f'Быков: {bulls} Коров: {cows}')
        steps += 1
    print(f'Вы выиграли за {steps} ходов')


main()
