from PIL import Image, ImageDraw

img = Image.new('RGB', (780, 500), color='#9932CC')
dr = ImageDraw.Draw(img)
dr.polygon(((50, 50), (170, 450), (290, 50), (240, 50), (170, 350), (100, 50)), fill='#7FFF00')
dr.rectangle(((320, 190), (350, 450)), fill='#B8860B')
dr.ellipse(((400, 320), (530, 450)), fill='white')
dr.ellipse(((430, 350), (500, 420)), fill='#9932CC')
dr.rectangle(((510, 320), (540, 450)), fill='white')
dr.ellipse(((590, 320), (720, 450)), fill='#ff0000')
dr.ellipse(((620, 350), (690, 420)), fill='#9932CC')
dr.rectangle(((700, 190), (730, 450)), fill='#ff0000')
del dr
img.save('name.png')

