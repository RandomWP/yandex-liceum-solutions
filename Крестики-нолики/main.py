def check_win(field):
    for line in field:
        if 'xxx' in ''.join(line):
            return 'x'
        elif '000' in ''.join(line):
            return '0'
    if field[0][0] == field[1][1] == field[2][2]:
        return field[0][0]
    elif field[0][2] == field[1][1] == field[2][0]:
        return field[0][2]
    return '-'


def transpon(matrix):
    return [[matrix[x][y] for x in range(0, len(matrix))] for y in range(len(matrix[0]))]


def tic_tac_toe(field):
    if check_win(field) != '-':
        print(check_win(field) + ' win')
        return
    elif check_win(transpon(field)) != '-':
        print(check_win(transpon(field)) + ' win')
        return
    print('draw')
