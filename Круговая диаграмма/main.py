from xlsxwriter import Workbook
from sys import stdin

with Workbook('res.xlsx') as workbook:
    worksheet = workbook.add_worksheet()
    row = 0
    for line in stdin:
        name, value = line.split()[0], int(line.split()[1])
        worksheet.write(row, 0, name)
        worksheet.write(row, 1, value)
        row += 1
    chart = workbook.add_chart({'type': 'pie'})
    chart.add_series({'categories': f'=Sheet1!A1:A{row}', 'values': f'=Sheet1!B1:B{row}'})
    worksheet.insert_chart('C3', chart)

