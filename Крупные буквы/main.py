letters = {
    'A': (' * ', '* *', '***', '* *', '* *'),
    'B': ('** ', '* *', '** ', '* *', '** '),
    'C': (' * ', '* *', '*  ', '* *', ' * ')
}

string = input()
print(*[('  '.join([letters[char][el] for char in string]) + '  ') for el in range(5)], sep='\n')
