def pitch_and_toss():
    from wave import open
    from struct import pack, unpack
    with open('in.wav', mode='rb') as source:
        params = source.getparams()
        fr_count = source.getnframes()
        data = unpack('<' + str(fr_count) + 'h', source.readframes(fr_count))
    q_size = fr_count // 4
    q1, q2, q3, q4 = data[:q_size], data[q_size:q_size * 2],\
        data[q_size * 2:q_size * 3], data[q_size * 3:]
    data = q3 + q4 + q1 + q2
    frames = pack('<' + str(len(data)) + 'h', *data)
    with open('out.wav', mode='wb') as out:
        out.setparams(params)
        out.writeframes(frames)
