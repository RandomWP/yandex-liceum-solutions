x = int(input())
y = int(input())
strings = [input() for _ in range(x)]
print(*[string[::2] for string in strings[::2]], sep='\n')
