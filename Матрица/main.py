def matrix(n=1, m=0, a=0):
    if m == 0:
        return [[a] * n for _ in range(n)]
    else:
        return [[a] * n for _ in range(m)]
