fak = input().split(', ')
dic = {}
for _ in range(len(fak)):
    temp = input()
    fakName = temp.split(':')[0][::-1]
    pupils = temp.split(': ')[1].split(', ')
    for pupil in pupils:
        if pupil not in dic:
            dic[pupil] = [fakName]
        else:
            dic[pupil] += [fakName]
            dic[pupil].sort()
plist = sorted(input().split(', '), reverse=True)
topPupil = plist[0]
for pup in plist:
    if len(dic[pup]) >= len(dic[topPupil]):
        topPupil = pup
print(topPupil + ': ', end='')
print(*dic[topPupil], sep=', ')
