def take_large_banknotes(banknotes):
    return [note for note in banknotes if note > 10]
