def make_preview(size, n_colors):
    from PIL import Image
    Image.open('image.jpg').resize(size).quantize(n_colors).save('res.bmp')
