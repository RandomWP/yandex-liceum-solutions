def is_shielded(string, pos):
    if string[pos - 1] != '\\':
        return False
    else:
        return not is_shielded(string, pos - 1)


def short_line(line):
    sspaces = len(line) - len(line.lstrip(' '))
    line = line.lstrip(' ')
    chPos = 0
    while chPos < len(line) - 1:
        if line[chPos] == '\'' and not is_shielded(line, chPos):
            for ch in range(len(line[chPos + 1:])):
                if line[chPos + 1:][ch] == '\'' and not is_shielded(line[chPos + 1:], ch):
                    chPos += ch + 1
                    break
        if line[chPos] == '#':
            chPos -= 1
            break
        elif line[chPos] == ' ' and line[chPos + 1] == ' ':
            line = line[:chPos] + line[chPos:].replace('  ', ' ', 1)
            chPos -= 1

        chPos += 1
    return ' ' * sspaces + line[:chPos + 1]


for _ in range(int(input())):
    print(short_line(input()))
