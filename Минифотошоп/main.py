def image_filter(pixels, i, j):
    ar, ag, ab = 0, 0, 0
    for row in range(5):
        for col in range(5):
            if row != i or col != j:
                ar += pixels[row, col][0]
                ag += pixels[row, col][1]
                ab += pixels[row, col][2]
    ar, ag, ab = ar // 25, ag // 25, ab // 25
    r = ar if not (ar + 30 < pixels[i, j][0] < ar - 30) else pixels[i, j]
    g = ag if not (ag + 30 < pixels[i, j][0] < ag - 30) else pixels[i, j]
    b = ab if not (ab + 30 < pixels[i, j][0] < ab - 30) else pixels[i, j]
    return r, g, b
