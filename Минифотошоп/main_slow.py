def image_filter(pixels, i, j):
    ar, ag, ab = 0, 0, 0
    row, col = 0, 0
    y = 0
    while True:
        try:
            _ = pixels[row, 0]
        except IndexError:
            break
        while True:
            try:
                if row != i or col != j:
                    ar += pixels[row, col][0]
                    ag += pixels[row, col][1]
                    ab += pixels[row, col][2]
            except IndexError:
                y = col
                col = 0
                break
            col += 1
        row += 1
    ar, ag, ab = ar // row * y, ag // row * y, ab // row * y
    r = ar if not (ar + 30 < pixels[i, j][0] < ar - 30) else pixels[i, j]
    g = ag if not (ag + 30 < pixels[i, j][0] < ag - 30) else pixels[i, j]
    b = ab if not (ab + 30 < pixels[i, j][0] < ab - 30) else pixels[i, j]
    return r, g, b
