from PIL import Image
from main_slo import image_filter

img = Image.open('image.jpg')
x, y = img.size
pixels = img.load()
rgb = [0, 0, 0]
print(dir(pixels))
for i in range(x):
    for j in range(y):
        pixels[i, j] = image_filter(pixels, i, j)
        print(i, j)
img.save('res.jpg')
