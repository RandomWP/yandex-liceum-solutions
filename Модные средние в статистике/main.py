values = sorted([int(el) for el in input().split()])
fashion = None
for val in values:
    if values.count(val) > values.count(fashion) or fashion is None:
        fashion = val
print(values[len(values) // 2], fashion)