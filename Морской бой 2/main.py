class SeaMap:
    def __init__(self):
        self.__field = [['.'] * 10 for _ in range(10)]

    def shoot(self, row, col, result):
        if result == 'miss':
            self.__field[row][col] = '*'
        elif result == 'hit':
            self.__field[row][col] = 'x'
        elif result == 'sink':
            self.__field[row][col] = 'X'
            self.__fix_sink(row, col)

    def __fix_sink(self, row, col):
        self.__field[row][col] = 'X'
        for i in range(-1, 2):
            for j in range(-1, 2):
                if i == j == 0 or not 0 <= row + i < 10 or not 0 <= col + j < 10:
                    continue
                if self.__field[row + i][col + j] == '.':
                    self.__field[row + i][col + j] = '*'
                elif self.__field[row + i][col + j] == 'x':
                    self.__fix_sink(row + i, col + j)

    def cell(self, row, col):
        return self.__field[row][col].lower()

