def input_field(size):
    return [input() for _ in range(size)]  # Построчный ввод поля


def print_field(field):
    for line in field:
        print(*line, sep='')


def vertical_mirror(field):
    return field[::-1]


def horizontal_mirror(field):
    return [line[::-1] for line in field]


def transponse(field):
    return [[field[x][y] for x in range(0, len(field))] for y in range(len(field[0]))]


f = input_field(int(input()))
print('Исходное поле')
print_field(f)
print('\nГоризонтальное отражение')
print_field(horizontal_mirror(f))
print('\nВертикальное отражение')
print_field(vertical_mirror(f))
print('\nТранспонирование')
print_field(transponse(f))
print('\nОтражение вдоль горизонтали и вертикали одновременно')
print_field(vertical_mirror(horizontal_mirror(f)))
print('\nГоризонтальное отражение, затем транспонирование')
print_field(transponse(horizontal_mirror(f)))
print('\nВертикальное отражение, затем транспонирование')
print_field(transponse(vertical_mirror(f)))
print('\nТранспонирование, затем два отражения')
print_field(vertical_mirror(horizontal_mirror(transponse(f))))
