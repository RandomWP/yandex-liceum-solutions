def password_level(pswd):
    if len(pswd) < 6:
        return 'Недопустимый пароль'
    if (pswd.islower() and pswd.isalpha()) or (pswd.isupper() and pswd.isalpha) or pswd.isdigit():
        return 'Ненадежный пароль'
    if ((pswd.islower() or pswd.isupper()) and pswd.isalpha) or \
            (not (pswd.islower() or pswd.isupper()) and (pswd.isalpha() or pswd.isdigit())):
        return 'Слабый пароль'
    if not (pswd.isalpha() or pswd.isdigit()) and not (pswd.islower() or pswd.isupper())):
        return 'Надежный пароль'
