catFlag = False
for _ in range(int(input())):
    string = input().lower()
    if 'кот' in string:
        catFlag = True
    elif 'пёс' in string:
        catFlag = False
print('МЯУ' if catFlag else 'НЕТ')
