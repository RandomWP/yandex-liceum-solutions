catFlag = False
for _ in range(int(input())):
    string = input().lower()
    if not catFlag:
        catFlag = 'кот' in string
print('МЯУ' if catFlag else 'НЕТ')
