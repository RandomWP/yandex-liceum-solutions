def dist_to_zero(x, y):
    from math import sqrt
    return sqrt(x * x + y * y)


def count_pi(iters):
    from random import random
    inCircle = 0
    for _ in range(iters):
        if dist_to_zero(random(), random()) < 1.0:
            inCircle += 1
    return inCircle * 4 / iters


print(count_pi(1000000))
