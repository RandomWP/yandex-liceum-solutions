bad = [name for name in input().split(',') if not all(ch.isalnum() or ch == '_' for ch in name)]
print(*[name.rjust(len(max(bad, key=len))) for name in sorted(bad)], sep='\n')
