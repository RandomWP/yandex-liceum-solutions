import random
random.seed()

heaps = [0, 0]
heaps[0] = int(input('Введите количество камней в первой куче\n'))
heaps[1] = int(input('Введите количество камней во второй куче\n'))


userTake = 0
while heaps[0] != 0 or heaps[1] != 0:
    if userTake == 0:
        if heaps[0] > heaps[1]:
            progTake = heaps[0] - heaps[1]
            progHeapPos = 0
        elif heaps[1] > heaps[0]:
            progTake = heaps[1] - heaps[0]
            progHeapPos = 1
        else:
            progTake = random.randint(1, heaps[0])
            progHeapPos = random.randint(0, 1)
    else:
        if userHeapPos == 0:
            if userTake <= heaps[1]:
                progHeapPos = 1
                progTake = userTake
            elif heaps[0] >= 0:
                progHeapPos = 1
                progTake = heaps[1] - 1
            else:
                progHeapPos = 0
                progTake = heaps[0]
        else:
            if userTake <= heaps[0]:
                progHeapPos = 0
                progTake = userTake
            elif heaps[0] >= 0:
                progHeapPos = 0
                progTake = heaps[0] - 1
            else:
                progHeapPos = 1
                progTake = heaps[1]

    heaps[progHeapPos] -= progTake

    print('Программа берёт:', progTake, 'камней из кучи', progHeapPos + 1)
    print('Осталось камней:', heaps[0], heaps[1])

    if heaps[0] <= 0 and heaps[1] <= 0:
        print('Программа выиграла!')
        break

    while True:
        userHeapPos = int(input('Из какой кучи вы хотите взять камень?\n'))
        if userHeapPos == 1 or userHeapPos == 2:
            userHeapPos -= 1
            break
        print('Вы можете взять камень только из 1-й или 2-й кучи!')
    while True:
        userTake = int(input('Сколько камней вы хотите взять?\n'))
        if userTake <= heaps[userHeapPos]:
            break
        print('Вы не можете взять больше камней, чем есть в куче!')
    heaps[userHeapPos] -= userTake

    print('Вы взяли:', userTake, 'камней из кучи', userHeapPos + 1)
    print('Осталось камней:', heaps[0], heaps[1])

    if heaps[0] <= 0 and heaps[1] <= 0:
        print('Вы выиграли!')
        break
