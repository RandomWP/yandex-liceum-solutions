def swap(first, second):
    temp = second[:]
    second.clear()
    second += first[:]
    first.clear()
    first += temp
