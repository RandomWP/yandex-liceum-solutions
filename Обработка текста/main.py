from sys import stdin
temp = enumerate(''.join(filter(lambda el: el not in '.,:;', stdin.read())).split())
words = []
for pair in temp:
    if pair[1] not in map(lambda el: el[1], words):
        words.append(pair)
print(*map(lambda el: f'{el[0]} - {el[1]}', sorted(filter(lambda el: el[1][0].isupper(), words),
                                                   key=lambda el: el[1])), sep='\n')
