def generation(line, gen=10):
    import numpy as np
    line = np.array(tuple(line), dtype=np.uint8)
    for _ in range(gen):
        neghbours_right = np.roll(line, -1)
        neghbours_left = np.roll(line, 1)
        line = ((line == 0) & neghbours_left & (neghbours_right == 0)) | \
               (line & neghbours_right & (neghbours_left == 0)) | \
               ((line == 0) & neghbours_right & (neghbours_left == 0)) | \
               (line & (neghbours_right == 0) & (neghbours_left == 0))
    return ''.join(line.astype('<U1'))

