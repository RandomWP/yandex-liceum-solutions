lst = [int(input()) for _ in range(int(input()))]
minV, maxV = int(input()), int(input())
print(*[el for el in lst if minV <= el <= maxV], sep='\n')
