from pymorphy2 import MorphAnalyzer
from sys import stdin
ma = MorphAnalyzer()


def smart_inflect(word_to_inflect, word):
    if word.tag.number == 'sing':
        return word_to_inflect.inflect({'sing', word.tag.gender}).word
    else:
        return word_to_inflect.inflect({'plur'}).word


for inp in stdin:
    word = ma.parse(inp.strip())[0]
    if word.tag.POS != 'NOUN':
        print('Не существительное')
        continue
    if word.tag.animacy == 'anim':
        print(smart_inflect(ma.parse('живое')[0], word).capitalize())
    else:
        print('Не', smart_inflect(ma.parse('живое')[0], word))

