def late(now, classes, bus):
    haveTime = (int(classes.split(':')[0]) * 60 + int(classes.split(':')[1])) - \
               (int(now.split(':')[0]) * 60 + int(now.split(':')[1]))
    bus = [b for b in bus if b >= 5 and b <= haveTime - 5 and
           (int(now.split(':')[0]) * 60 + int(now.split(':')[1]) + b) <=
           (int(classes.split(':')[0]) * 60 + int(classes.split(':')[1])) - 15]
    return f'Выйти через {bus[-1] - 5} минут' if bus else 'Опоздание'
