def input_graph():
    data = []
    while True:
        lineData = tuple(map(int, input().split()))
        if len(lineData) == 2:
            break
        data.append(lineData)
    size = max(map(lambda el: max(el[:2]), data))
    graph = [[0] * size for _ in range(size)]
    for x, y, ln in data:
        graph[x - 1][y - 1], graph[y - 1][x - 1] = ln, ln
    return graph, lineData


def bfs(start, graph):
    dist = [10 ** 10] * len(graph)
    dist[start] = 0
    p = [0] * len(graph)
    p[start] = start
    queue = []
    queue.insert(0, start)
    while queue:
        currNode = queue.pop()
        for i in range(len(graph)):
            if graph[currNode][i] == 0:
                continue
            if dist[i] > dist[currNode] + graph[currNode][i]:
                dist[i] = dist[currNode] + graph[currNode][i]
                p[i] = currNode
                queue.insert(0, i)
    return p


def way(dest, p):
    if p[dest] != dest:
        return way(p[dest], p) + [dest]
    return [dest]


def main():
    graph, pos = input_graph()
    print(*map(lambda el: el + 1, way(pos[1] - 1, bfs(pos[0] - 1, graph))), sep=', ')


main()
