lst = [int(el) for el in input().split()]
start, stop = input().split()
print(sum([el ** 2 for el in lst[int(start):int(stop) + 1]]))
