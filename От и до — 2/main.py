lst = [int(el) for el in input().split()]
start, stop = input().split()
print(sum(lst[int(start):int(stop) + 1]))
