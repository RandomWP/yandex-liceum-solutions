anagrams = {}
for _ in range(int(input())):
    word = input().lower().strip()
    letterSet = ''.join((sorted(word)))
    if letterSet in anagrams:
        anagrams[letterSet].add(word)
    else:
        anagrams[letterSet] = {word}

for ls in sorted(filter(lambda el: len(el) > 1, anagrams.values()), key=lambda el: sorted(el)):
    print(*sorted(ls))
