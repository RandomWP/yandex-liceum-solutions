def palindrome(s):
    return 'Палиндром' if s.lower().replace(' ', '') == s.lower().replace(' ', '')[::-1] \
        else 'Не палиндром'
