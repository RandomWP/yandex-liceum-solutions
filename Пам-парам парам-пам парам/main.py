def count_vowels(st):
    return sum(map(lambda l: st.count(l), 'уеыаоэяию'))


string = input()
vowels = count_vowels(string.split()[0])
print('Парам пам-пам'
      if all(map(lambda st: count_vowels(st) == vowels, string.split()))
      else 'Пам парам')
