def find_middle(list):
    return sorted(list)[len(list) // 2]


def find_fashion(list):
    fashion = None
    for val in list:
        if list.count(val) > list.count(fashion) or fashion is None:
            fashion = val
    return fashion


values = [sorted([int(val) for val in input().split()]) for _ in range(int(input()))]
middles = [find_middle(vlist) for vlist in values]
fashions = [find_fashion(vlist) for vlist in values]
values = sorted([var for vlist in values for var in vlist])
print(*middles)
print(*fashions)
print(find_middle(middles), find_fashion(fashions),
      find_middle(values), find_fashion(values), sep='\n')
