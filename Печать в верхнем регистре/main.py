def print_upper(*args):
    default_print = print
    default_print(*map(lambda arg: str(arg).upper(), args))


print = print_upper

