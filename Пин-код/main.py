def is_prime(number):
    from math import sqrt
    if number == 1:
        return False
    for d in range(2, int(sqrt(number)) + 1):
        if number % d == 0:
            return False
    return True


def is_palindrome(number):
    return str(number) == str(number)[::-1]


def is_bin_exp(number):
    from math import log2
    return log2(number) % 1 == 0


def check_pin(pin):
    pin = [int(num) for num in pin.split('-')]
    return 'Корректен' if (is_prime(pin[0]) and is_palindrome(pin[1]) and is_bin_exp(pin[2])) \
        else 'Некорректен'
