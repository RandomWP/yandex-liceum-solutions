height = int(input())
space = height - 1
signs = 1
for _ in range(height):
    print(' ' * space, '*' * signs, sep='')
    signs += 2
    space -= 1