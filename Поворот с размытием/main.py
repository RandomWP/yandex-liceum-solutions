def motion_blur(n):
    from PIL import Image, ImageFilter
    Image.open('image.jpg').transpose(Image.ROTATE_270).\
        filter(ImageFilter.GaussianBlur(n)).save('res.jpg')
