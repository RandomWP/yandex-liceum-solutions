from PIL import Image, ImageDraw


class ImageDrawer(ImageDraw.ImageDraw):
    def up_arrow(self, xy, fill=None, outline=None):
        x1, y1, x2, y2 = xy
        h, w = y2 - y1, x2 - x1
        self.polygon(((x1, y1 + h * 0.2), (x1 + w * 0.5, y1),
                      (x2, y1 + h * 0.2), (x1 + w * 0.8, y1 + h * 0.2),
                      (x1 + w * 0.8, y2), (x1 + w * 0.2, y2),
                      (x1 + w * 0.2, y1 + h * 0.2)), fill=fill, outline=outline)

    def down_arrow(self, xy, fill=None, outline=None):
        x1, y1, x2, y2 = xy
        h, w = y2 - y1, x2 - x1
        self.polygon(((x1, y1 + h * 0.8), (x1 + w * 0.5, y2),
                      (x2, y1 + h * 0.8), (x1 + w * 0.8, y1 + h * 0.8),
                      (x1 + w * 0.8, y1), (x1 + w * 0.2, y1),
                      (x1 + w * 0.2, y1 + h * 0.8)), fill=fill, outline=outline)
