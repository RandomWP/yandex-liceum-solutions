arr = [5, 136, 7, 666]
sortArr = arr.sort()
sortedArr = sorted(arr)  # В разные переменные запишем результаты работы функции и метода
print(sortArr)  # Метод ничего не возвращает,
print(arr)  # но изменяет текущий список
print(sortedArr)  # Функция возвращает новый объект,
print(arr is sortedArr)  # который не идентичен начальному
