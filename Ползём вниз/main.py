string = input()
symb = string[0]
spaces = 0
line = symb
for el in string[1:]:
    if el == '>':
        line += symb
        spaces += 1
    elif el == '<':
        line = line[1:] + symb
        spaces -= 1
    elif el == 'V':
        print(line)
        line = ' ' * spaces + symb
print(line)
