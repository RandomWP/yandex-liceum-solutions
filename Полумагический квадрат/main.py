from sys import stdin
sq = tuple(tuple(map(int, line.split())) for line in stdin)
sq += tuple(zip(*sq))
print('YES' if all(map(lambda el: sum(el) == sum(sq[0]), sq)) else 'NO')
