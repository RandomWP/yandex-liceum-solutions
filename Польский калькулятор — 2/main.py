from math import factorial

data = input().split()
stack = []

ops = {
    '+': lambda x, y: y + x,
    '-': lambda x, y: y - x,
    '*': lambda x, y: y * x,
    '/': lambda x, y: y // x,
    '~': lambda x: -x,
    '!': lambda x: factorial(x)
}

for el in data:
    if el in '+-*/~!#@':
        if el in '+-*/':
            stack.append(ops[el](stack.pop(), stack.pop()))
        elif el in '~!':
            stack.append(ops[el](stack.pop()))
        elif el == '#':
            stack.append(stack[-1])
        elif el == '@':
            a, b, c = stack.pop(), stack.pop(), stack.pop()
            stack.append(b)
            stack.append(a)
            stack.append(c)
    else:
        stack.append(int(el))
print(*stack)
