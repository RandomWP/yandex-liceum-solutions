class Server:
    def __init__(self, name):
        self._name = name
        self._emails = []

    def get_name(self):
        return self._name

    def add_email(self, email):
        self._emails.append(email)

    def get_emails_for_receiver(self, receiver):
        return tuple(filter(lambda email: email.get_receiver() == receiver, self._emails))

    def del_emails_for_receiver(self, receiver):
        self._emails = list(
            filter(lambda email: email.get_receiver() != receiver, self._emails))

    def print_emails(self):
        for email in self._emails:
            print_email(email)


class User:
    def __init__(self, name):
        self._name = name
        self._emails = []

    def get_name(self):
        return self._name


class MailClient:
    def __init__(self, server, user):
        self._server = server
        self._user = user

    def get_server(self):
        return self._server

    def get_user(self):
        return self._user

    def send_mail(self, server, user, message):
        server.add_email(message)

    def receive_mail(self):
        emails = self._server.get_emails_for_receiver(self._user)
        self._server.del_emails_for_receiver(self._user)
        return emails


class Email:
    def __init__(self, sender, receiver, text):
        self._sender = sender
        self._receiver = receiver
        self._text = text

    def get_sender(self):
        return self._sender

    def get_receiver(self):
        return self._receiver

    def get_text(self):
        return self._text


def print_email(email):
    print('From: ' + email.get_sender().get_name())
    print('To: ' + email.get_receiver().get_name() + '\n')
    print(email.get_text() + '\n')


def main():
    servers, users = {}, {}
    print('Добро пожаловать в почтовую систему!')
    while True:
        print('Что вы хотите сделать?')
        print('1 - добавить пользователя')
        print('2 - добавить сервер')
        print('3 - подключить учётную запись пользователя к серверу')
        print('4 - отправить сообщение')
        print('5 - получить почту')
        print('6 - просмотреть список серверов')
        print('7 - просмотреть список пользователей')
        print('0 - выйти')
        cmd = input('> ')
        if cmd not in ('1', '2', '3', '4', '5', '6', '7', '0'):
            print('Неверная команда!')
        elif cmd == '1':
            name = input('Введите имя пользователя: ')
            users[name] = User(name)
        elif cmd == '2':
            name = input('Введите название сервера: ')
            servers[name] = Server(name)
        elif cmd == '3':
            user_name = input('Введите имя пользователя: ')
            server_name = input('Введите название сервера: ')
            if user_name not in users or server_name not in servers:
                print('Сервер или пользователь не существует')
                break
            curr_client = MailClient(servers[server_name], users[user_name])
        elif cmd == '4':
            server_name = input('Введите название сервера: ')
            user_name = input('Введите имя пользователя: ')
            if user_name not in users or server_name not in servers:
                print('Сервер или пользователь не существует')
                continue
            text = input('Введите текст сообщения: ')
            curr_client.send_mail(servers[server_name], users[user_name], Email(
                curr_client.get_user(), users[user_name], text))
        elif cmd == '5':
            for email in curr_client.receive_mail():
                print_email(email)
        elif cmd == '6':
            print('Доступные сервера: ')
            print(*servers, sep='\n')
        elif cmd == '7':
            print('Пользователи: ')
            print(*users, sep='\n')
        elif cmd == '0':
            break
        print('\n')


if __name__ == '__main__':
    main()
