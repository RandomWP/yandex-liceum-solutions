strings = [input() for _ in range(int(input()))]
searchStrings = [input() for _ in range(int(input()))]
print(*[string for string in strings if all(sStr in string for sStr in searchStrings)], sep='\n')
