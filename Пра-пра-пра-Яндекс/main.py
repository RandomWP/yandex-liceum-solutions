strings = [input() for _ in range(int(input()))]
searchString = input()
print(*[string for string in strings if searchString in string], sep='\n')
