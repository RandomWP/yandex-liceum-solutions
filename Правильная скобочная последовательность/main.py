def bracket_check(test_string):
    br = 0
    for char in test_string:
        br += 1 if char == '(' else -1
        if br < 0:
            print('NO')
            return
    print('YES' if br == 0 else 'NO')
