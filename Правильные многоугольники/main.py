from PIL import Image, ImageDraw


class MyImageDraw(ImageDraw.ImageDraw):
    def regular_polygon(self, center_coords, sides, radius, rotation=0, fill=None, outline=None):
        from math import pi, sin, cos
        angles = map(lambda n: 2 * pi * n / sides + rotation, range(sides))
        corners = map(lambda angle: (round(sin(angle) * radius) + center_coords[0],
                                     round(cos(angle) * radius) + center_coords[1]),
                      angles)
        self.polygon(tuple(corners), fill=fill, outline=outline)
        self.ellipse((center_coords[0] - radius, center_coords[1] - radius,
                      center_coords[0] + radius, center_coords[1] + radius), outline=outline)

