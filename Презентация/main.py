from pptx import Presentation, util

data = (('''betavariate''', '''Help on method betavariate in module random:

betavariate(alpha, beta) method of random.Random instance
    Beta distribution.
    
    Conditions on the parameters are alpha > 0 and beta > 0.
    Returned values range between 0 and 1.
'''), ('''choice''', '''Help on method choice in module random:

choice(seq) method of random.Random instance
    Choose a random element from a non-empty sequence.
'''), ('''choices''', '''Help on method choices in module random:

choices(population, weights=None, *, cum_weights=None, k=1) method of random.Random instance
    Return a k sized list of population elements chosen with replacement.
    
    If the relative weights or cumulative weights are not specified,
    the selections are made with equal probability.
'''), ('''expovariate''', '''Help on method expovariate in module random:

expovariate(lambd) method of random.Random instance
    Exponential distribution.
    
    lambd is 1.0 divided by the desired mean.  It should be
    nonzero.  (The parameter would be called "lambda", but that is
    a reserved word in Python.)  Returned values range from 0 to
    positive infinity if lambd is positive, and from negative
    infinity to 0 if lambd is negative.
'''), ('''gammavariate''', '''Help on method gammavariate in module random:

gammavariate(alpha, beta) method of random.Random instance
    Gamma distribution.  Not the gamma function!
    
    Conditions on the parameters are alpha > 0 and beta > 0.
    
    The probability distribution function is:
    
                x ** (alpha - 1) * math.exp(-x / beta)
      pdf(x) =  --------------------------------------
                  math.gamma(alpha) * beta ** alpha
'''), ('''gauss''', '''Help on method gauss in module random:

gauss(mu, sigma) method of random.Random instance
    Gaussian distribution.
    
    mu is the mean, and sigma is the standard deviation.  This is
    slightly faster than the normalvariate() function.
    
    Not thread-safe without a lock around calls.
'''), ('''getrandbits''', '''Help on built-in function getrandbits:

getrandbits(...) method of random.Random instance
    getrandbits(k) -> x.  Generates an int with k random bits.
'''), ('''getstate''', '''Help on method getstate in module random:

getstate() method of random.Random instance
    Return internal state; can be passed to setstate() later.
'''), ('''lognormvariate''', '''Help on method lognormvariate in module random:

lognormvariate(mu, sigma) method of random.Random instance
    Log normal distribution.
    
    If you take the natural logarithm of this distribution, you'll get a
    normal distribution with mean mu and standard deviation sigma.
    mu can have any value, and sigma must be greater than zero.
'''), ('''normalvariate''', '''Help on method normalvariate in module random:

normalvariate(mu, sigma) method of random.Random instance
    Normal distribution.
    
    mu is the mean, and sigma is the standard deviation.
'''), ('''paretovariate''', '''Help on method paretovariate in module random:

paretovariate(alpha) method of random.Random instance
    Pareto distribution.  alpha is the shape parameter.
'''), ('''randint''', '''Help on method randint in module random:

randint(a, b) method of random.Random instance
    Return random integer in range [a, b], including both end points.
'''), ('''random''', '''Help on built-in function random:

random(...) method of random.Random instance
    random() -> x in the interval [0, 1).
'''), ('''randrange''', '''Help on method randrange in module random:

randrange(start, stop=None, step=1, _int=<class 'int'>) method of random.Random instance
    Choose a random item from range(start, stop[, step]).
    
    This fixes the problem with randint() which includes the
    endpoint; in Python this is usually not what you want.
'''), ('''sample''', '''Help on method sample in module random:

sample(population, k) method of random.Random instance
    Chooses k unique random elements from a population sequence or set.
    
    Returns a new list containing elements from the population while
    leaving the original population unchanged.  The resulting list is
    in selection order so that all sub-slices will also be valid random
    samples.  This allows raffle winners (the sample) to be partitioned
    into grand prize and second place winners (the subslices).
    
    Members of the population need not be hashable or unique.  If the
    population contains repeats, then each occurrence is a possible
    selection in the sample.
    
    To choose a sample in a range of integers, use range as an argument.
    This is especially fast and space efficient for sampling from a
    large population:   sample(range(10000000), 60)
'''), ('''seed''', '''Help on method seed in module random:

seed(a=None, version=2) method of random.Random instance
    Initialize internal state from hashable object.
    
    None or no argument seeds from current time or from an operating
    system specific randomness source if available.
    
    If *a* is an int, all bits are used.
    
    For version 2 (the default), all of the bits are used if *a* is a str,
    bytes, or bytearray.  For version 1 (provided for reproducing random
    sequences from older versions of Python), the algorithm for str and
    bytes generates a narrower range of seeds.
'''), ('''setstate''', '''Help on method setstate in module random:

setstate(state) method of random.Random instance
    Restore internal state from object returned by getstate().
'''), ('''shuffle''', '''Help on method shuffle in module random:

shuffle(x, random=None) method of random.Random instance
    Shuffle list x in place, and return None.
    
    Optional argument random is a 0-argument function returning a
    random float in [0.0, 1.0); if it is the default None, the
    standard random.random will be used.
'''), ('''triangular''', '''Help on method triangular in module random:

triangular(low=0.0, high=1.0, mode=None) method of random.Random instance
    Triangular distribution.
    
    Continuous distribution bounded by given lower and upper limits,
    and having a given mode value in-between.
    
    http://en.wikipedia.org/wiki/Triangular_distribution
'''), ('''uniform''', '''Help on method uniform in module random:

uniform(a, b) method of random.Random instance
    Get a random number in the range [a, b) or [a, b] depending on rounding.
'''), ('''vonmisesvariate''', '''Help on method vonmisesvariate in module random:

vonmisesvariate(mu, kappa) method of random.Random instance
    Circular data distribution.
    
    mu is the mean angle, expressed in radians between 0 and 2*pi, and
    kappa is the concentration parameter, which must be greater than or
    equal to zero.  If kappa is equal to zero, this distribution reduces
    to a uniform random angle over the range 0 to 2*pi.
'''), ('''weibullvariate''', '''Help on method weibullvariate in module random:

weibullvariate(alpha, beta) method of random.Random instance
    Weibull distribution.
    
    alpha is the scale parameter and beta is the shape parameter.
'''))

prs = Presentation()
templ = prs.slide_layouts[1]

for name, txt in data:
    slide = prs.slides.add_slide(templ)
    title = slide.shapes.title
    text = slide.shapes[1]
    title.text = name
    text.text = txt
    text.text_frame.paragraphs[0].font.size = util.Pt(15)
    text.text_frame.paragraphs[0].font.name = 'Courier New'
prs.save('prs.pptx')

