class BaseCommand:
    def condition(self, num):
        return True

    def operation(self, num):
        return num

    def __call__(self, numbers):
        return map(lambda el: self.operation(el) if self.condition(el) else el, numbers)


class MakeNegative(BaseCommand):
    def condition(self, num):
        return num > 0

    def operation(self, num):
        return -num


class Square(BaseCommand):
    def operation(self, num):
        return num * num


class StrangeCommand(BaseCommand):
    def condition(self, num):
        return not num % 5

    def operation(self, num):
        return num + 1


CMD_NAMES = {'make_negative': MakeNegative(), 'square': Square(),
             'strange_command': StrangeCommand()}


def main():
    numbers = map(int, input().split())
    for _ in range(int(input())):
        numbers = CMD_NAMES[input()](numbers)
    print(*numbers)


if __name__ == '__main__':
    main()
