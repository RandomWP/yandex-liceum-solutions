from docx import Document
from sys import stdin

place = input()
time = input()

doc = Document()

for name in stdin:
    doc.add_heading('Приглашение', 0)
    p = doc.add_paragraph('Приглашается уважаемая ')
    p.add_run(name).bold = True
    p.add_run(' на мероприятие к празднику 8 марта.')
    doc.add_paragraph().add_run('Развлечения гарантированны!').italic = True
    doc.add_paragraph()
    doc.add_paragraph(f'Мероприятие состоится {time.lower()} {place.lower()}')
    doc.add_page_break()
doc.save('Ivites.docx')

