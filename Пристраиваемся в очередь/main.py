queue = []
for _ in range(int(input())):
    temp = input()
    if 'встал' in temp:
        queue.append(temp[:temp.find(' встал')])
    elif 'Привет' in temp:
        queue.insert(queue.index(temp[temp.find(', ') + 2:temp.find('!')]) + 1,
                     temp[temp.find('! ') + 2:temp.find(' будет')])
    elif 'хватит тут стоять' in temp:
        queue.remove(temp[:temp.find(', ')])
print(*queue, sep='\n')
