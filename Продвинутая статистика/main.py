def sample(x):
    return len(set(x)) <= 10


def advanced_stat(strings, func=sample):
    return [string.upper() for string in strings if func(string)]
