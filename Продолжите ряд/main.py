def continue_fibonacci_sequence(sequence, n):
    for i in range(n):
        next_element = sequence[-1] + sequence[-2]
        # sequence = sequence + [next_element]
        # каждый раз содается новый объект sequence и исходный не изменяется
        sequence += [next_element]
        # Значения добавляются в объект sequence, новый объект не создается
