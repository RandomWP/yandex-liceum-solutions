def mrg(image1, image2):
    from PIL import ImageMath, Image
    bands1 = image1.split()
    bands2 = image2.split()
    res_bands = []
    for col in range(3):
        res_bands.append(ImageMath.eval("convert(float(a)*0.5 + float(b)*0.5, 'L')",
                                        a=bands1[col], b=bands2[col]))
    return Image.merge('RGB', res_bands)


def transparency(filename1, filename2):
    from PIL import Image
    mrg(Image.open(filename1), Image.open(filename2)).save('res.jpg')
