def markdown_to_docx(text):
    from docx import Document
    lines = text.split('\n')
    filename = lines[0].strip()
    lines = map(lambda line: line.rstrip(), lines[1:])
    doc = Document()
    doc.add_heading(filename, 0)
    for line in lines:
        if not line:
            doc.add_paragraph()
        elif line.startswith('#'):
            doc.add_heading(line.strip('# '), len(line.split(' ')[0]))
        elif line[0].isdigit():
            doc.add_paragraph(line.lstrip('1234567890.'), style='List Number')
        elif line[0] in '+*-' and line[1] == ' ':
            doc.add_paragraph(line.lstrip('-+*'), style='List Bullet')
        elif line[0] in '*_':
            count = line[:3].count(line[0])
            run = doc.add_paragraph().add_run(line.strip('*_ '))
            if count == 1 or count == 3:
                run.italic = True
            if count == 2 or count == 3:
                run.bold = True
        else:
            doc.add_paragraph(line.strip())
    doc.save('res.docx')

