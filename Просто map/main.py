def simple_map(transformation, values):
    return [transformation(value) for value in values]
