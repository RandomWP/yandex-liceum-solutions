def prime(number):
    from math import sqrt
    for d in range(2, int(sqrt(number)) + 1):
        if number % d == 0:
            return 'Составное число'
    return 'Простое число'
