stones = int(input('Введите начальное количество камней\n'))
userTake = 0
while stones > 0:
    # Program math block
    if userTake == 0:
        progTake = 1 if stones % 4 == 0 else stones % 4
    elif stones <= 3:
        progTake = stones
    else:
        progTake = 4 - userTake
    stones -= progTake

    # Win checking
    if stones <= 0:
        print('Программа выиграла!')
        break

    print('Программа берёт:', progTake, 'камней')
    print('Осталось камней:', stones)

    # User input block
    while True:
        userTake = int(input('Сколько камней вы хотите взять?\n'))
        if userTake <= 3 and userTake <= stones and userTake > 0:
            stones -= userTake
            break
        print('Можно брать не меньше 1 и не больше 3 камней и не больше, чем камней в куче!')

    # Win checking
    if stones <= 0:
        print('Вы выиграли!')
        break

    print('Осталось камней', stones)
