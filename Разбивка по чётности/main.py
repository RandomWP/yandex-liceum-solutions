class OddEvenSeparator:
    def __init__(self):
        self.__odd = []
        self.__even = []

    def add_number(self, num):
        if not (num % 2):
            self.__even.append(num)
        else:
            self.__odd.append(num)

    def even(self):
        return self.__even

    def odd(self):
        return self.__odd

