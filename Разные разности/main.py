values = [int(input()) for _ in range(int(input()))]
out = set()
for val1 in values:
    for val2 in values:
        out.add(val1 - val2)
print(*sorted(list(out)), sep='\n')
