class SparseArray:
    def __init__(self):
        self.__arr = dict()

    def __getitem__(self, key):
        return self.__arr.get(key, 0)

    def __setitem__(self, key, value):
        self.__arr[key] = value

