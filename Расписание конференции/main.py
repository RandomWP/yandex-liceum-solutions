class Conf:
    def __init__(self, start, end, topic):
        self._start = start
        self._end = end
        self._topic = topic
        self._reports = []

    def get_start(self):
        return self._start

    def get_end(self):
        return self._end

    def get_topic(self):
        return self._topic

    def get_reports(self):
        return self._reports

    def add_report(self, report):
        self._reports.append(report)


class Report:
    def __init__(self, start, duration, topic):
        self._start = start
        self._duration = duration
        self._topic = topic

    def get_start(self):
        return self._start

    def get_duration(self):
        return self._duration

    def get_end(self):
        return self._start + self._duration

    def get_topic(self):
        return self._topic


def check_report(conf, report):
    if report.get_start() < conf.get_start():
        print('Доклад не может начаться раньше конференции!')
        return False
    if report.get_end() > conf.get_end():
        print('Доклад не может закончится позже конца конференции!')
        return False
    for rep in conf.get_reports():
        if rep.get_start() < report.get_start() < rep.get_end() or \
                rep.get_start() < report.get_end() < rep.get_end() or \
                report.get_start() < rep.get_start() < report.get_end() or \
                report.get_start() < rep.get_end() < report.get_end():
            print(f'Доклад пересекается с докладом на тему {rep.get_topic()}!')
            return False
    return True


conf = Conf(int(input('Введите время начала конференции (0 - 24): ')),
            int(input('Введите время конца конференции: ')), input('Введите тему конференции: '))

while True:
    new_rep = Report(int(input('Введите время начала доклада (0 - 24): ')), int(input(
        'Введите продолжительность доклада (в часах): ')), input('Введите тему доклада: '))
    if check_report(conf, new_rep):
        conf.add_report(new_rep)
        print('Доклад успешно добавлен!')
