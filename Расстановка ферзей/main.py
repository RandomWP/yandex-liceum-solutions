def queens(size):
    solutions = [[]]
    for row in range(size):
        solutions = add_queen(row, size, solutions)
    for solution in solutions:
        print(*map(lambda el: el + 1, solution), sep='')


def add_queen(row, size, prev_sol):
    return [sol + [col] for sol in prev_sol for col in range(size) if no_conflict(row, col, sol)]


def no_conflict(row, col, solution):
    return all(solution[r] != col and
               solution[r] + r != col + row and
               solution[r] - r != col - row
               for r in range(row))

