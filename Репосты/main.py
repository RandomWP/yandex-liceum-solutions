posts = {}


def add_views(name, views):
    global posts
    posts[name][0] += views
    if posts[name][1] is not None:
        add_views(posts[name][1], views)


def add_post(name, views, repost=None):
    global posts
    posts[name] = [0, repost]
    add_views(name, views)


for _ in range(int(input())):
    temp = input().split()
    if 'опубликовал' in temp:
        add_post(temp[0], int(temp[-1]))
    elif 'отрепостил' in temp:
        add_post(temp[0], int(temp[-1]), temp[4][:-1])
for key in posts:
    print(posts[key][0])
