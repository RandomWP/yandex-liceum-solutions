def eratosthenes(n):
    out = []
    nums = list(range(2, n + 1))
    while nums:
        k = nums[0]
        for el in nums:
            if el % k == 0:
                out.append(el)
                allPrime = False
        nums = [el for el in nums if el not in out]
        del out[out.index(k)]
    print(*out)


eratosthenes(1000)