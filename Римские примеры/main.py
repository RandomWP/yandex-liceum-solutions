def to_roman(num):
    result = ''
    for arab, rom in ((1000, 'M'), (900, 'CM'), (500, 'D'), (400, 'CD'), (100, 'C'), (90, 'XC'),
                      (50, 'L'), (40, 'XL'), (10, 'X'), (9, 'IX'), (5, 'V'), (4, 'IV'), (1, 'I')):
        result += num // arab * rom
        num %= arab
    return result


def roman():
    global three
    three = one + two
    print(f'{to_roman(one)} + {to_roman(two)} = {to_roman(three)}')
