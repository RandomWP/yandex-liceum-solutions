class Person:
    def __init__(self, first_name, middle_name, last_name, phones):
        self.__first_name = first_name
        self.__middle_name = middle_name
        self.__last_name = last_name
        self.__phones = phones

    def get_phone(self):
        return self.__phones.get('private')

    def get_name(self):
        return f'{self.__last_name} {self.__first_name} {self.__middle_name}'

    def get_work_phone(self):
        return self.__phones.get('work')

    def get_sms_text(self):
        return f'Уважаемый {self.__first_name} {self.__middle_name}! ' \
            f'Примите участие в нашем беспроигрышном конкурсе для физических лиц'


class Company:
    def __init__(self, name, tp, phones, *employers):
        self.__name = name
        self.__tp = tp
        self.__phones = phones
        self.__employers = employers

    def get_phone(self):
        if 'contact' in self.__phones:
            return self.__phones.get('contact')
        for emp in self.__employers:
            if emp.get_work_phone():
                return emp.get_work_phone()
        return

    def get_name(self):
        return self.__name

    def get_sms_text(self):
        return f'Для компании {self.__name} есть супер предложение! ' \
            f'Примите участие в нашем беспроигрышном конкурсе для {self.__tp}'


def send_sms(*args):
    for arg in args:
        if arg.get_phone():
            print(f'Отправлено СМС на номер {arg.get_phone()} c текстом: {arg.get_sms_text()}')
        else:
            print(f'Не удалось отправить сообщение абоненту: {arg.get_name()}')

