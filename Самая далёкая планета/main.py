def find_farthest_orbit(list_of_orbits):
    from math import pi
    list_of_orbits = list(filter(lambda orb: orb[0] != orb[1], list_of_orbits))
    orb_dic = {pi * orb[0] * orb[1]: orb for orb in list_of_orbits}
    return orb_dic[max(orb_dic)]
