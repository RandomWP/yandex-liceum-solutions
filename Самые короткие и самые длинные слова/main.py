class MinMaxWordFinder:
    def __init__(self):
        self.__shortest = []
        self.__longest = []

    def add_sentence(self, sent):
        for word in sent.split():
            if not self.__shortest and not self.__longest:
                self.__shortest.append(word)
                self.__longest.append(word)
                continue
            if len(word) > len(self.__longest[0]):
                self.__longest = [word]
            elif len(word) == len(self.__longest[0]) and word not in self.__longest:
                self.__longest.append(word)
            if len(word) < len(self.__shortest[0]):
                self.__shortest = [word]
            elif len(word) == len(self.__shortest[0]):
                self.__shortest.append(word)
        self.__longest.sort()
        self.__shortest.sort()

    def shortest_words(self):
        return self.__shortest

    def longest_words(self):
        return self.__longest

