from functools import reduce
from sys import stdin
print(reduce(lambda val1, val2: val1 if val1 < val2 else val2, stdin.read().split('\n')))

