def number_in_english(n):
    if n == 0:
        return 'zero'
    return {
        0: '',
        1: 'one',
        2: 'two',
        3: 'three',
        4: 'four',
        5: 'five',
        6: 'six',
        7: 'seven',
        8: 'eight',
        9: 'nine'
    }[n // 100] + (' hundred and ' if n % 100 > 0 and n > 99 else ' hundred' if n > 99 else '') + (
        {
            10: 'ten',
            11: 'eleven',
            12: 'twelve',
            13: 'thirteen',
            14: 'fourteen',
            15: 'fifteen',
            16: 'sixteen',
            17: 'seventeen',
            18: 'eighteen',
            19: 'nineteen'
        }[n - n // 100 * 100] if 9 < n % 100 < 20 else {
            0: '',
            2: 'twenty',
            3: 'thirty',
            4: 'forty',
            5: 'fifty',
            6: 'sixty',
            7: 'seventy',
            8: 'eighty',
            9: 'ninety'
        }[(n - n // 100 * 100) // 10] + (' ' if n % 10 and (n - n // 100 * 100) > 9 else '') + {
            0: '',
            1: 'one',
            2: 'two',
            3: 'three',
            4: 'four',
            5: 'five',
            6: 'six',
            7: 'seven',
            8: 'eight',
            9: 'nine'
        }[n % 10])
