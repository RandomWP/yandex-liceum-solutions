class Polynomial:
    def __init__(self, coefficients):
        self.__coefficients = coefficients

    def __getitem__(self, key):
        return self.__coefficients[key]

    def __len__(self):
        return len(self.__coefficients)

    def __add__(self, other):
        temp = []
        for n in range(len(max(self, other, key=len))):
            try:
                temp.append(self[n] + other[n])
            except IndexError:
                temp.append(max(self, other, key=len)[n])
        return Polynomial(temp)

    def __call__(self, x):
        return sum([el * x ** n for n, el in enumerate(self)])

