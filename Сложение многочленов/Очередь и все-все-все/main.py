class Queue:
    def __init__(self, *values):
        self.__queue = list(values)

    @property
    def values(self):
        return self.__queue

    def append(self, *values):
        self.__queue += values

    def copy(self):
        return Queue(*self.values)

    def pop(self):
        return self.__queue.pop(0)

    def extend(self, queue):
        self += queue

    def __add__(self, other):
        return Queue(*self.values + other.values)

    def __iadd__(self, other):
        self.__queue += other.values
        return self

    def __eq__(self, other):
        return self.values == other.values

    def __rshift__(self, shift):
        return Queue(*self.values[shift:])

    def __str__(self):
        return '[' + ' -> '.join(map(str, self.values)) + ']'

    def next(self):
        return Queue(*self.values[1:])

    def __next__(self):
        return self.next()

