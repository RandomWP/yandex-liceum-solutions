class ChessPiece:
    def __init__(self, row, col, color):
        self._row = row
        self._col = col
        self._color = color

    def set_position(self, row, col):
        self._row = row
        self._col = col

    def check_board(self, row, col):
        return 0 <= row < 8 and 0 <= col < 8

    def char(self):
        return self.__class__.__name__[0]

    def get_color(self):
        return self._color

    def can_move(self, row, col):
        return self.check_board(row, col)


class Bishop(ChessPiece):
    def can_move(self, row, col):
        return self.check_board(row, col) and abs(self._row - row) == abs(self._col - col)

