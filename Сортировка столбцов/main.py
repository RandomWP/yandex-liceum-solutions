def super_sort(rows, cols):
    import numpy as np
    from itertools import cycle
    A = np.random.randint(1, 100, (rows, cols))
    B = A.copy()
    B = B.transpose()
    for trig, col in zip(cycle((False, True)), range(len(B))):
        B[col].sort()
        if trig:
            B[col] = B[col][::-1]
    return A, B.transpose()

