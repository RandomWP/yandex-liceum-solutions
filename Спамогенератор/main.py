def print_spam_letter(name, date, email, place='Город N'):
    print(f'To: {email}')
    print(f'Здравствуйте, {name}!')
    print(f'Были бы рады видеть вас на встрече начинающих программистов '
          f'в {place}, которая пройдет {date}.')


print_spam_letter(email='maksim@tipoemail.com', name='Макс',
                  place='Город F, ул.Пушкина, д. Колотушкина', date='31 февраля')

print_spam_letter(email='nekit1337@yundexmail.com', name='Никита', date='-11 марта')
