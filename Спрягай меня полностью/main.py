from pymorphy2 import MorphAnalyzer
word = MorphAnalyzer().parse(input())[0]
if word.tag.POS == 'INFN' or word.tag.POS == 'VERB':
    print('Прошедшее время:')
    for gend in ('masc', 'femn', 'neut', 'plur'):
        print(word.inflect({'past', gend}).word)
    print('Настоящее время:')
    for person in ('1per', '2per', '3per'):
        for number in ('sing', 'plur'):
            print(word.inflect({'pres', person, number}).word)
else:
    print('Не глагол')
