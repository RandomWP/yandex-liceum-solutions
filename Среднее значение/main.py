def print_average(arr):
    if not arr:
        print(0)
        return
    print(sum(arr) / len(arr))
