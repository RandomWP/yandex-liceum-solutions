from PIL import Image

img = Image.open('image.jpg')
x, y = img.size
pixels = img.load()
rgb = [0, 0, 0]
for i in range(x):
    for j in range(y):
        for el in range(3):
            rgb[el] += pixels[i, j][el]
print(*tuple(map(lambda el: el // (x * y), rgb)))
