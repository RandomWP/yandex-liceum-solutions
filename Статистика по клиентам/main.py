operations = {}


def get_transactions(t):
    global operations
    if t == 'print_it':
        for op in operations:
            print(operations[op][0], op, operations[op][1])
    else:
        opName = t.split('-')[1].split(':')[0]
        opSum = int(t.split('-')[1].split(':')[1])
        if opName in operations:
            operations[opName][0] += 1
            operations[opName][1] += opSum
        else:
            operations[opName] = [1, opSum]
