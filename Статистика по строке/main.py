def fun_string_stat(str_to_analize, str_in, str_out):
    return str_in in str_to_analize and str_out not in str_to_analize
