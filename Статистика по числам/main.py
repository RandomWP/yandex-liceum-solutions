def fun_ints_stat(n1, n2, *args):
    out = len(tuple(filter(lambda num: num % n1 == 0 and num % n2 != 0, args)))
    return out, len(args) == out
