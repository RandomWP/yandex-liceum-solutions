class IntSeq(list):
    def add_number(self, num):
        self.append(num)


class MinStat(IntSeq):
    def result(self):
        if not self:
            return
        return min(self)


class MaxStat(IntSeq):
    def result(self):
        if not self:
            return
        return max(self)


class AverageStat(IntSeq):
    def result(self):
        if not self:
            return
        return sum(self) / len(self)

