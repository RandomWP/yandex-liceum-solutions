def print_statistics(arr):
    if not arr:
        for _ in range(5):
            print(0)
        return
    arr.sort()
    print(len(arr))
    print(sum(arr) / len(arr))
    print(float(min(arr)))
    print(float(max(arr)))
    print(float(arr[len(arr) // 2] if len(arr) % 2 != 0 else
          (arr[len(arr) // 2] + arr[len(arr) // 2 - 1]) / 2))
