values = [int(el) for el in input().split()]
a, b = (int(el) for el in input().split())
values = [el for el in values if not (a <= el <= b)]
print(sum(values))
