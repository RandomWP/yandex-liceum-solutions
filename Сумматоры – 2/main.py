class Summator:
    def transform(self, n):
        return n

    def sum(self, N):
        if N == 1:
            return 1
        return self.sum(N - 1) + self.transform(N)


class PowerSummator(Summator):
    def __init__(self, power):
        self.__power = power

    def transform(self, n):
        return n ** self.__power


class SquareSummator(PowerSummator):
    def __init__(self):
        super().__init__(2)


class CubeSummator(PowerSummator):
    def __init__(self):
        super().__init__(3)
