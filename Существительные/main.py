from pymorphy2 import MorphAnalyzer
from sys import stdin

words = ''.join((map(lambda el: el if el.isalnum() else ' ', stdin.read()))).split()
freq = {}
morph = MorphAnalyzer()
for word in words:
    anls = morph.parse(word)[0]
    if not (anls.tag.POS == 'NOUN' and anls.score > 0.5):
        continue
    if anls.normal_form in freq:
        freq[anls.normal_form] += 1
    else:
        freq[anls.normal_form] = 1
print(*tuple(map(lambda w: w[0], sorted(freq.items(), key=lambda w: (w[1], w), reverse=True)))[:10])
