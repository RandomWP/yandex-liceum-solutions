

def is_lucky(num):
    num = str(num).zfill(6)
    return sum([int(dig) for dig in list(num[0:3])]) == sum([int(dig) for dig in list(num[3:6])])


def lucky(ticket):
    if is_lucky(lastTicket) and is_lucky(ticket):
        return 'Счастливый'
    return 'Несчастливый'
