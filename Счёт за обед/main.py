def count_food(days):
    global daily_food
    return sum([daily_food[day - 1] for day in days])
