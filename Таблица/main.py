class Table:
    def __init__(self, rows, cols):
        self.__table = [[0] * cols for _ in range(rows)]
        self.__rows = rows
        self.__cols = cols

    def get_value(self, row, col):
        if row not in range(self.__rows) or col not in range(self.__cols):
            return
        return self.__table[row][col]

    def set_value(self, row, col, value):
        self.__table[row][col] = value

    def n_rows(self):
        return self.__rows

    def n_cols(self):
        return self.__cols

