from math import sqrt

funcs = {'x': '___', 'sqrt_fun': 'sqrt(___)'}
for _ in range(int(input())):
    raw = input().split()
    if raw[0] == 'define':
        new_func = raw[2:]
        for name, func in funcs.items():
            new_func = tuple(map(lambda el: f'({func})' if el == name else el, new_func))
        funcs[raw[1]] = ' '.join(new_func)
    elif raw[0] == 'calculate':
        args = raw[2:]
        func = tuple()
        for arg in args:
            eval(f"print({funcs[raw[1]].replace('___', arg)}, end=' ')")
        print()

