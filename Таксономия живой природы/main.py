class Organism:
    pass


class Acellularia(Organism):
    pass


class Cellularia(Organism):
    pass


class Prokaryota(Cellularia):
    pass


class Eukaryota(Cellularia):
    pass


class Unicellularia(Eukaryota):
    pass


class Fungi(Eukaryota):
    pass


class Plantae(Eukaryota):
    pass


class Animalia(Eukaryota):
    pass

