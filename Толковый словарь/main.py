dict = {}
for _ in range(int(input())):
    temp = input()
    dict[temp[:temp.find(' ')]] = temp[temp.find(' ') + 1:]
print(*[dict.get(input(), 'Нет в словаре') for _ in range(int(input()))], sep='\n')
