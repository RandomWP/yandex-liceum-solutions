class Point:
    def __init__(self, x, y):
        self.__coords = [x, y]

    @property
    def coords(self):
        return self.__coords

    def __eq__(self, other):
        return self.coords == other.coords

    def __ne__(self, other):
        return not self == other

