def transpose(matrix):
    from copy import deepcopy
    tr = deepcopy(matrix)
    matrix.clear()
    matrix += [[tr[x][y] for x in range(len(tr))] for y in range(len(tr[0]))]
