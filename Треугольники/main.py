class Triangle:
    def __init__(self, a, b, c):
        self.__sides = (a, b, c)

    def perimeter(self):
        return sum(self.__sides)


class EquilateralTriangle(Triangle):
    def __init__(self, side):
        super().__init__(side, side, side)

