IQSum = 0
for i in range(1, int(input()) + 1):
    IQ = int(input())
    IQSum += IQ
    if IQ > IQSum / i:
        print('>')
    elif IQ < IQSum / i:
        print('<')
    else:
        print(0)