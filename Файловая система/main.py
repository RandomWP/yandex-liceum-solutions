def split_path(path):
    return path.split('/')


def join_path(path_tuple):
    return '/'.join(path_tuple)


class Directory:
    def __init__(self, name, content=''):
        self._content = {}
        self._name = name
        if content:
            self.add_content(content)

    def add_content(self, path, file_content=''):
        if not path:
            raise ValueError('Path is empty')
        path = split_path(path)
        name = path[0]
        if name in self._content and len(path) == 1:
            raise FileExistsError('File or directory exists')
        if '.' in name:
            self._content[name] = File(name)
        else:
            self._content[name] = Directory(name, join_path(path[1:]))

    def edit_file(self, path, file_content):
        if not path or not file_content:
            raise ValueError('Path or file content is empty')
        path = split_path(path)
        if path[0] not in self._content:
            raise FileNotFoundError('File or directory not found')
        if '.' in path[0]:
            self._content[path[0]].set_content(file_content)
        else:
            self._content[path[0]].edit_file(join_path(path[1:]))

    def get_file_or_dir(self, path):
        if not path:
            raise ValueError('Path is empty')
        path = split_path(path)
        if path[0] not in self._content:
            raise FileNotFoundError('Path is incorrrect')
        if len(path) == 1:
            return self._content[path[0]]
        else:
            return self._content[path[0]].get_file_or_dir(join_path(path[1:]))

    def print_content(self):
        print('Files: ')
        for name in self._content:
            if '.' in name:
                print(name)
        print('\nSubdirs: ')
        for name in self._content:
            if '.' not in name:
                print(name)

    def del_file_or_dir(self, path):
        if not path:
            raise ValueError('Path is empty')
        path = split_path(path)
        if path[0] not in self._content:
            raise FileNotFoundError('Path is incorrect')
        if len(path) == 1:
            del self._content[path[0]]


class File:
    def __init__(self, content=''):
        self._content = ''

    def set_content(self, content):
        self._content = content

    def print_content(self):
        print(self._content)


def main():
    root = Directory('root')
    print('Welcome to Python virtual file system!')
    print('Type "help" to get help')
    while True:
        command = (input('> ') + '\t').split('\t')
        if not command:
            continue
        cmd = command[0]
        args = command[1:]
        if cmd not in ('help', 'mk', 'write', 'rm', 'read', 'exit'):
            print('Command not found, use "help" to view a commands list')
            continue
        if cmd == 'help':
            print('Avaliable commands:')
            print('help - show this help')
            print('mk [directory or file path] - create new directory or file '
                  '(use name with extension to create file)')
            print('write [path] [file content] - write content into a file')
            print('rm [path] - delete directory or path')
            print('read [path] - show file or directory content (list of files and subdirectories)')
            print('exit - escape from this hell')
            print('All arguments must be separated by tabs, names and paths can not contain tabs')
        elif cmd == 'mk':
            try:
                root.add_content(args[0])
            except ValueError:
                print('Path is empty!')
                continue
            except FileExistsError:
                print('File or directory already exists')
                continue
        elif cmd == 'write':
            try:
                root.edit_file(args[0], args[1])
            except ValueError:
                print('Path is empty!')
            except FileNotFoundError:
                print('Path is incorrect!')
        elif cmd == 'rm':
            try:
                obj = root.del_file_or_dir(args[0])
            except ValueError:
                print('Path is empty!')
            except FileNotFoundError:
                print('Path is incorrect!')
            del obj
        elif cmd == 'read':
            if not args[0]:
                root.print_content()
                continue
            try:
                root.get_file_or_dir(args[0]).print_content()
            except ValueError:
                print('Path is empty!')
            except FileNotFoundError:
                print('Path is incorrect!')
        elif cmd == 'exit':
            return


if __name__ == '__main__':
    main()
