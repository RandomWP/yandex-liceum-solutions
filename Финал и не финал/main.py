from math import ceil
teams = {int(input()): input() for _ in range(int(input()))}
scores = sorted(teams.keys(), reverse=True)
print(*sorted([teams[score] for score in scores[:ceil(len(scores) / 2)]]), sep='\n')
print(*sorted([teams[score] for score in scores[ceil(len(scores) / 2):]]), sep='\n')
