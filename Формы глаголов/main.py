import pymorphy2
from sys import stdin

morph = pymorphy2.MorphAnalyzer()
print(len(tuple(filter(lambda el: morph.parse(el)[0].normal_form in
                ('видеть', 'увидеть', 'глядеть', 'примечать', 'узреть'),
                       ''.join((map(lambda el: el if el.isalnum() or el in '\n\t ' else '',
                                    stdin.read()))).split()))))
