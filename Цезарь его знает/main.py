alphabetLower = 'абвгдежзийклмнопрстуфхцчшщъыьэюя'
alphabetUpper = alphabetLower.upper()
step = int(input())
message = input()
code = ''
for char in message:
    if char not in alphabetLower + alphabetUpper:
        code += char
        continue
    if ord(char) + step > ord('я') and char in alphabetLower:
        code += chr(ord(char) + step - 32)
    elif ord(char) + step > ord('Я') and char in alphabetUpper:
        code += chr(ord(char) + step - 32)
    else:
        code += chr(ord(char) + step)
print(code)