counter = 0
num = int(input())
while num != 1:
    if num % 2 == 0:
        num //= 2
    else:
        num -= 1
    counter += 1
print(counter)