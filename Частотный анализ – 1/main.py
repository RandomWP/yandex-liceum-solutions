strings = []
for _ in range(int(input())):
    string = input().lower()
    for char in ',.!?:;':
        string = string.replace(char, '')
    strings += string.split()
words = {}
for word in set(strings):
    if strings.count(word) not in words.keys():
        words[strings.count(word)] = [word]
    else:
        words[strings.count(word)] += [word]
    words[strings.count(word)].sort()
for key in sorted(list(words.keys()), reverse=True):
    print(*[word.capitalize() for word in words[key]], sep='\n')
