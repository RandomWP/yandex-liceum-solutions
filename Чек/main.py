def export_check(text):
    from xlsxwriter import Workbook
    with Workbook('res.xlsx') as workbook:
        worksheet = workbook.add_worksheet()
        for row, line in enumerate(text.split('\n')):
            for col, value in enumerate(line.strip().split('\t')):
                worksheet.write(row, col, (int(value) if value.isdigit() else value))
            worksheet.write(row, col + 1, '=B' + str(row + 1) + '*C' + str(row + 1))
        worksheet.write(row + 1, 0, 'Итого')
        worksheet.write(row + 1, col + 1, '=SUM(D1:D' + str(row + 1) + ')')

