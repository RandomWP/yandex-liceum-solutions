def chip_and_dale(number):
    from wave import open
    from struct import pack, unpack
    with open('in.wav', mode='rb') as source:
        params = source.getparams()
        fr_count = source.getnframes()
        data = unpack('<' + str(fr_count) + 'h', source.readframes(fr_count))
    data = data[::number]
    frames = pack('<' + str(len(data)) + 'h', *data)
    with open('out.wav', mode='wb') as out:
        out.setparams(params)
        out.writeframes(frames)
