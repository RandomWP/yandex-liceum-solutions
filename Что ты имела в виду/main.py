numbers = [2, 5, 7, 7, 8, 4, 1, 6]
# odd = even = [] - odd и even ссылаются на один и тот же объект и сортировка не работает
# Нужно:
odd = []
even = []
for number in numbers:  # Проходим по числам в списке numbers
    if number % 2 == 0:
        even.append(number)  # Четные числа добавляем в список even
    else:
        odd.append(number)  # Нечетные числа добавляем в список odd
