import numpy as np


def bw_convert():
    from PIL import Image
    img = np.asarray(Image.open('image.jpg'))
    img = np.apply_along_axis(normalize, axis=2, arr=img)
    Image.fromarray(img).save('res.jpg')


coeff = np.array([0.2989, 0.5870, 0.1140], dtype=np.float)


def normalize(pix):
    c = np.uint8(np.round(np.sum(pix * coeff)))
    return c, c, c

