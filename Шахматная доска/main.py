def board(num, size):
    from PIL import Image, ImageDraw
    res = num * size
    img = Image.new('RGB', (res, res), (255, 255, 255))
    dr = ImageDraw.Draw(img)
    for x in range(0, res, size * 2):
        for y in range(0, res, size * 2):
            dr.rectangle((x, y, x + size - 1, y + size - 1), fill='black')
            dr.rectangle((x + size, y + size, x + size * 2 - 1, y + size * 2 - 1), fill='black')
    img.save('res.png', 'PNG')


board(4, 30)
