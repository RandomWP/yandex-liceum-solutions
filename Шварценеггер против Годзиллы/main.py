from math import gcd

nums = []
dens = []
num = 0
den = 1
for i in range(int(input())):
    nums.append(int(input()))
    dens.append(int(input()))
    den *= dens[i]
#for i in dens:
#    den *= i
for i in range(len(nums)):
    num += nums[i] * (den // dens[i])
print(num, den)
print(num // gcd(num, den), den // gcd(num, den), sep='/')
