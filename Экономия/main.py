size = int(input())
matrix = [[0] * size]
for lineNum in range(size - 1):
    line = [int(val) for val in input().split()]
    line += [0] * (size - lineNum - 1)
    matrix.append(line)
for x in range(size):
    for y in range(size):
        matrix[x][y] = matrix[y][x]
start, des = (int(val) for val in input().split())
econ = start
for st in range(size):
    if st == start:
        continue
    if matrix[start][st] + matrix[st][des] < matrix[start][econ] + matrix[econ][des]:
        econ = st
print(econ)
