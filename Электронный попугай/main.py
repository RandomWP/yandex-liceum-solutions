usedPhrases = set()


def parrot(phrase):
    global usedPhrases
    if phrase in usedPhrases:
        print(phrase)
    else:
        usedPhrases.add(phrase)
