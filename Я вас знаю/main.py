username = ''


def polite_input(question):
    global username
    if not username:
        username = input('Как вас зовут?\n')
    return input(f'{username}, {question}\n')
