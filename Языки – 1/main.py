pupils = set()
for i in range(int(input()) + int(input())):
    temp = input()
    if temp in pupils:
        pupils.remove(temp)
    else:
        pupils.add(temp)
print(len(pupils) if len(pupils) else 'NO')
