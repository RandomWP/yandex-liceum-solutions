pupils = set()
pupils2lang = set()
for i in range(int(input()) + int(input()) + int(input())):
    temp = input()
    if temp in pupils and temp in pupils2lang:
        pupils2lang.remove(temp)
    elif temp in pupils:
        pupils2lang.add(temp)
    else:
        pupils.add(temp)
print(len(pupils2lang) if len(pupils2lang) else 'NO')